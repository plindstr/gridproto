/**
 * Utility methods.
 * 
 * Possibly broken. Likely hairy.
 * 
 */

module util {

    export function addStyleName(className: string, element: HTMLElement): void {
        element.classList.add(className);
    }

    export function removeStyleName(className: string, element: HTMLElement): void {
        element.classList.remove(className);
    }

    export function hasStyleName(className: string, element: HTMLElement): boolean {
        return element.classList.contains(className);
    }

    /**
     * Code pulled from StackOverflow for procedurally generating CSS.
     * Not used at the moment, but left in nonetheless.
     */
    export function setCSS(selector: string, style: string) {
        if (!document.styleSheets) {
            return;
        }

        if (document.getElementsByTagName("head").length == 0) {
            return;
        }

        var styleSheet: any;
        var mediaType: any;
        if (document.styleSheets.length > 0) {
            for (var i = 0; i < document.styleSheets.length; i++) {
                if (document.styleSheets[i].disabled) {
                    continue;
                }
                var media: any = document.styleSheets[i].media;
                mediaType = typeof media;

                if (mediaType == "string") {
                    if (media == "" || (media.indexOf("screen") != -1)) {
                        styleSheet = document.styleSheets[i];
                    }
                } else if (mediaType == "object") {
                    if (media.mediaText == "" || (media.mediaText.indexOf("screen") != -1)) {
                        styleSheet = document.styleSheets[i];
                    }
                }

                if (typeof styleSheet != "undefined") {
                    break;
                }
            }
        }

        if (typeof styleSheet == "undefined") {
            var styleSheetElement = document.createElement("style");
            styleSheetElement.type = "text/css";

            document.getElementsByTagName("head")[0].appendChild(styleSheetElement);

            for (i = 0; i < document.styleSheets.length; i++) {
                if (document.styleSheets[i].disabled) {
                    continue;
                }
                styleSheet = document.styleSheets[i];
            }

            var media = styleSheet.media;
            mediaType = typeof media;
        }

        if (mediaType == "string") {
            for (i = 0; i < styleSheet.rules.length; i++) {
                if (styleSheet.rules[i].selectorText && styleSheet.rules[i].selectorText.toLowerCase() == selector.toLowerCase()) {
                    styleSheet.rules[i].style.cssText = style;
                    return;
                }
            }

            styleSheet.addRule(selector, style);
        } else if (mediaType == "object") {
            for (i = 0; i < styleSheet.cssRules.length; i++) {
                if (styleSheet.cssRules[i].selectorText && styleSheet.cssRules[i].selectorText.toLowerCase() == selector.toLowerCase()) {
                    styleSheet.cssRules[i].style.cssText = style;
                    return;
                }
            }

            styleSheet.insertRule(selector + "{" + style + "}", 0);
        }
    }

    /**
     * Inner function for drilling down the node structure, searching for a node of a certain type.
     * Not guaranteed to work in every use-case...
     */
    export function findElementDown(type: string, target: any): HTMLElement {
        if (target == null) return;
        var elem: HTMLElement = <HTMLElement>target;

        try {
            while (elem.nodeName != type) {
                elem = elem.parentElement;
                if (elem == null) {
                    console.log("Could not find " + type + " element");
                    return null;
                }
            }

            return elem;
        } catch (e) {
            console.log("Unexpected error in findElement: " + e);
            return null;
        }

    }
    
    export function findElementUp(type: string, target: any):HTMLElement {

        if(target == null) return null;
        var elem: HTMLElement = <HTMLElement>target;

        if(elem == null) return null;
        
        if(elem.nodeName == type) return elem;
        
        try {
            for(var i = 0, l = elem.childElementCount; i < l; ++i) {
                var n = findElementUp(type,elem.childNodes[i]);
                if(n) return n;
            }
        } catch(e) {
            return null;
        }
        
        return null;
    }
    
    export function findElementUpDown(type: string, target: any):HTMLElement {
        return findElementUp(type,target) || findElementDown(type,target) || null;
    }

}
