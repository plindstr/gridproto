/**
 * Grid table implementation
 * using a real HTML table structure
 * 
 * DANGER, WILL ROBINSON!
 * 
 * This code can be pretty fragile.
 *
 */

module grid {

    export interface Touch {
        identifier: number;
        target: EventTarget;
        screenX: number;
        screenY: number;
        clientX: number;
        clientY: number;
        pageX: number;
        pageY: number;
    };

    export interface TouchList {
        length: number;
        item(index: number): Touch;
        identifiedTouch(identifier: number): Touch;
    };

    export interface TouchEvent extends UIEvent {
        touches: TouchList;
        targetTouches: TouchList;
        changedTouches: TouchList;
        altKey: boolean;
        metaKey: boolean;
        ctrlKey: boolean;
        shiftKey: boolean;
    };

    /**
     * Identifier used by event callbacks to identify target of received event
     */
    export enum EventDestination {
        HEADER,
        FOOTER,
        BODY
    };

    /**
     * HTML table abstraction
     */
    export class Table {

        private _rows: number;
        private _cols: number;

        private _wrapper: HTMLDivElement;
        private _table: HTMLTableElement;
        private _header: HTMLElement;
        private _footer: HTMLElement;
        private _body: HTMLElement;
        private _rowData: HTMLTableRowElement[];
        private _cellData: HTMLTableCellElement[][];
        private _headerData: HTMLTableHeaderCellElement[][];
        private _footerData: HTMLTableCellElement[];

        private _columnSortable: boolean[];

        private _clickListeners: Function[];
        private _mouseDownListeners: Function[];
        private _mouseUpListeners: Function[];
        private _mouseMoveListeners: Function[];
        private _touchStartListeners: Function[];
        private _touchMoveListeners: Function[];
        private _touchEndListeners: Function[];

        private _keyboardListeners: Function[]; // TODO: get rid of this...

        private _keyPressListeners: Function[];
        private _keyDownListeners: Function[];
        private _keyUpListeners: Function[];

        private _focusListeners: Function[];
        private _blurListeners: Function[];

        public constructor(cols: number, rows: number = 1,
            cellInitializer: (element: HTMLElement, col: number, row: number) => void =
            () => { },
            headerInitializer: (element: HTMLElement, col: number) => void =
            null,
            footerInitializer: (element: HTMLElement, col: number) => void =
            null) {

            this._rows = 0;
            this._cols = cols;
            this._rowData = [];
            this._cellData = [];

            this._headerData = [];
            this._footerData = [];

            this._columnSortable = new Array<boolean>(cols);
            for (var i = 0; i < cols; ++i) {
                this._columnSortable[i] = true;
            }

            this._clickListeners = [];

            this._mouseDownListeners = [];
            this._mouseUpListeners = [];
            this._mouseMoveListeners = [];

            this._keyboardListeners = [];

            this._keyPressListeners = [];
            this._keyDownListeners = [];
            this._keyUpListeners = [];

            this._focusListeners = [];
            this._blurListeners = [];

            this._touchStartListeners = [];
            this._touchMoveListeners = [];
            this._touchEndListeners = [];

            this._table = document.createElement('table');

            this._header = document.createElement('thead');

            this._footer = document.createElement('tfoot');
            this._body = document.createElement('tbody');

            util.addStyleName('v-grid-header', this._header);
            util.addStyleName('v-grid-footer', this._footer);
            util.addStyleName('v-grid-body', this._body);

            if (headerInitializer) {
                this.addHeader(headerInitializer);
                this._table.appendChild(this._header);
            }

            if (footerInitializer) {
                this.addFooter(footerInitializer);
                this._table.appendChild(this._footer);
            }

            for (var i = 0; i < rows; ++i) {
                this.addRow(cellInitializer);
                this._table.appendChild(this._body);
            }



            ////////////////////////////
            // Event handling section //
            ////////////////////////////


            // Mouse handling
            ///////////////////


            // Add header click listener
            this._header.onclick = (e) => {

                // Extract header data cell
                var elem = util.findElementDown('TH', e.target);
                if (!elem) {
                    console.log("Disregarded header click with invalid target");
                    return;
                }

                // Figure out coordinates

                var col: number = -1, row: number = -1;

                for (var i = 0, l = this._headerData.length; i < l; ++i) {
                    var idx = this._headerData[i].indexOf(<HTMLTableHeaderCellElement>elem);
                    if (idx > -1) {
                        col = idx;
                        row = i;
                        break;
                    }
                }

                if (col > -1 && row > -1) {

                    console.log("Click on header cell " + col + "," + row);

                    // Call all registered listeners
                    this._clickListeners.forEach((fn) => {
                        fn.call(this, EventDestination.HEADER, elem, col, row, e);
                    });

                }

            };

            // Add footer click listener
            this._footer.onclick = (e) => {

                // Extract footer data cell
                var elem = util.findElementDown('TD', e.target);
                if (!elem) {
                    console.log("Disregarded footer click with invalid target");
                    return;
                }

                // Figure out coordinates
                var col: number = this._footerData.indexOf(<HTMLTableCellElement>elem);

                console.log("Click on footer cell " + col);

                // Call all registered listeners
                this._clickListeners.forEach((fn) => {
                    fn.call(this, EventDestination.FOOTER, elem, col, 0, e);
                });

            };

            // Add body click listener
            this._body.onclick = (e) => {

                // Extract data cell
                var cell = util.findElementDown('TD', e.target);
                var rowElement = util.findElementDown('TR', cell);

                if (!cell || !rowElement) {
                    console.log("Disregarded body click with invalid target");
                    return;
                }

                // Figure out coordinates
                var row: number = this._rowData.indexOf(<HTMLTableRowElement>rowElement);
                var col: number = this._cellData[row].indexOf(<HTMLTableCellElement>cell);

                console.log("Click on table cell (" + col + ", " + row + ")");

                // Call all registered listeners
                this._clickListeners.forEach((fn) => {
                    fn.call(this, EventDestination.BODY, cell, col, row, e);
                });

            };

            this._table.onmousedown = (e) => {
                console.log("Mouse: mousedown");

                this._mouseDownListeners.forEach((l) => {
                    l.call(this, e);
                });
            }

            this._table.onmouseup = (e) => {
                console.log("Mouse: mouseup");

                this._mouseUpListeners.forEach((l) => {
                    l.call(this, e);
                });
            }

            this._table.onmousemove = (e) => {
                console.log("Mouse: mousemove");

                this._mouseMoveListeners.forEach((l) => {
                    l.call(this, e);
                });
            }


            // Keyboard handling
            //////////////////////


            this._table.tabIndex = 1;
            this._table.onkeydown = (e) => {
                var code = e.keyCode;

                console.log("Keyboard: keydown " + code);

                // Call low-level keyboard event listeners
                this._keyboardListeners.forEach((listener) => {
                    listener.call(this, e);
                });
            }

            this._table.onkeyup = (e) => {
                var code = e.keyCode;

                console.log("Keyboard: keyup " + code);

                // Call low-level keyboard event listeners
                this._keyboardListeners.forEach((listener) => {
                    listener.call(this, e);
                });
            }

            this._table.onkeypress = (e) => {
                var code = e.keyCode;

                console.log("Keyboard: keypress " + code);

                // Call low-level keyboard event listeners
                this._keyboardListeners.forEach((listener) => {
                    listener.call(this, e);
                });
            }


            // Focus handling
            ///////////////////

            this._table.onfocus = (e) => {
                console.log("Table gained focus");

                this._focusListeners.forEach((listener) => {
                    listener.call(this, e);
                });
            }

            this._table.onblur = (e) => {
                console.log("Table lost focus");

                this._blurListeners.forEach((listener) => {
                    listener.call(this, e);
                });
            }

            this._table.addEventListener("touchstart", (e: TouchEvent) => {
                console.log("touchstart event");
                this._touchStartListeners.forEach((l) => {
                    l.call(this, e);
                });
            });

            this._table.addEventListener("touchmove", (e: TouchEvent) => {
                console.log("touchmove event");
                this._touchMoveListeners.forEach((l) => {
                    l.call(this, e);
                });
            });

            this._table.addEventListener("touchend", (e: TouchEvent) => {
                console.log("touchend event");
                this._touchEndListeners.forEach((l) => {
                    l.call(this, e);
                });
            });

            // Define final DOM properties
            ////////////////////////////////

            //   this._table.style.width = "100%";
            //    this._table.style.height = "100%";

            this._wrapper = document.createElement('div');
            util.addStyleName('v-grid-wrapper', this._wrapper);
            this._wrapper.appendChild(this._table);
        }

        /**
         * Add high-level click listener
         */
        public addClickListener(fn: (target: EventDestination, cell: HTMLTableCellElement, column: number, row: number, event: MouseEvent) => void): void {
            this._clickListeners.push(fn);
        }

        /**
         * Add a low-level keyboard listener
         */
        public addKeyboardListener(fn: (event: KeyboardEvent) => void): void {
            this._keyboardListeners.push(fn);
        }

        /**
         * Add a focus listener (called when the table receives focus)
         */
        public addFocusListener(fn: (event: FocusEvent) => void): void {
            this._focusListeners.push(fn);
        }

        /**
         * Add a blur listener (called when the table loses focus)
         */
        public addBlurListener(fn: (event: FocusEvent) => void): void {
            this._blurListeners.push(fn);
        }

        /**
         * Add a low-level mouse down listener
         */
        public addMouseDownListener(fn: (event: MouseEvent) => void): void {
            this._mouseDownListeners.push(fn);
        }

        /**
         * Add a low-level mouse up listener
         */
        public addMouseUpListener(fn: (event: MouseEvent) => void): void {
            this._mouseUpListeners.push(fn);
        }

        /**
         * Add a low-level mouse move listener
         */
        public addMouseMoveListener(fn: (event: MouseEvent) => void): void {
            this._mouseMoveListeners.push(fn);
        }

        public addTouchStartListener(fn: (event: TouchEvent) => void): void {
            this._touchStartListeners.push(fn);
        }

        public addTouchMoveListener(fn: (event: TouchEvent) => void): void {
            this._touchMoveListeners.push(fn);
        }

        public addTouchEndListener(fn: (event: TouchEvent) => void): void {
            this._touchEndListeners.push(fn);
        }

        /**
         * Add a header row
         * TODO: current model is only equipped to handle one header row!
         */
        public addHeader(headerInitializer: (element: HTMLElement, col: number) => void = () => { }): void {
            var row: HTMLTableHeaderCellElement[] = [];
            var tr = document.createElement('tr');
            util.addStyleName('v-grid-row', tr);
            util.addStyleName('v-grid-header-row', tr);

            for (var i = 0; i < this._cols; ++i) {
                var th = document.createElement('th');
                util.addStyleName('v-grid-cell', th);
                util.addStyleName('v-grid-header-cell', th);

                headerInitializer(th, i);

                tr.appendChild(th);
                row.push(th);
            }

            this._headerData.push(row);
            this._header.appendChild(tr);
        }

        public setColumnSortable(column: number, sortable: boolean) {
            if (column < 0 || column > this._cols) throw "No such column " + column;
            this._columnSortable[column] = sortable;
        }

        public isColumnSortable(column: number) {
            if (column < 0 || column > this._cols) throw "No such column " + column;
            return this._columnSortable[column];
        }

        /**
         * Add a footer row
        * TODO: current model is only equipped to handle one footer row! 
        */
        public addFooter(footerInitializer: (element: HTMLElement, col: number) => void = () => { }): void {
            var tr = document.createElement('tr');
            util.addStyleName('v-grid-row', tr);
            util.addStyleName('v-grid-footer-row', tr);

            for (var i = 0; i < this._cols; ++i) {
                var td = document.createElement('td');
                util.addStyleName('v-grid-cell', td);
                util.addStyleName('v-grid-footer-cell', td);

                footerInitializer(td, i);

                tr.appendChild(td);
                this._footerData.push(td);
            }

            this._footer.appendChild(tr);
        }

        /**
         * Add a table row
         */
        public addRow(cellInitializer: (element: HTMLElement, col: number, row: number) => void = () => { }): void {

            var arr: HTMLTableCellElement[] = [];
            var tr = document.createElement('tr');
            util.addStyleName('v-grid-row', tr);
            util.addStyleName(this._rows & 1 ? 'v-grid-row-odd' : 'v-grid-row-even', tr);

            for (var i = 0; i < this._cols; ++i) {
                var td = document.createElement('td');
                util.addStyleName('v-grid-cell', td);

                cellInitializer(td, i, this._rows);

                tr.appendChild(td);
                arr.push(td);
            }

            this._rows++;
            this._body.appendChild(tr);
            this._rowData.push(tr);
            this._cellData.push(arr);
        }

        /**
         * Get direct reference to a table cell
         */
        public getCell(col: number, row: number): HTMLTableCellElement {
            if (row >= this._rows) throw "No such row " + row + " (max " + (this._rows - 1) + ")";
            if (col >= this._cols) throw "No such column " + col + " (max " + (this._cols - 1) + ")";
            return this._cellData[row][col];
        }

        /**
         * Get coordinates for cell
         */
        public getCellCoordinates(cell: HTMLTableCellElement): { x: number; y: number; } {

            var rowElement = util.findElementDown('TR', cell);

            if (!cell || !rowElement) {
                console.log("Disregarded body click with invalid target");
                return;
            }

            // Figure out coordinates
            var row: number = this._rowData.indexOf(<HTMLTableRowElement>rowElement);
            var col: number = this._cellData[row].indexOf(<HTMLTableCellElement>cell);

            return { x: col, y: row };

        }

        public getRowCoordinate(row: HTMLTableRowElement): number {
            return this._rowData.indexOf(row);
        }
        
        /**
         * Get a direct reference to a table row element
         */
        public getRow(row: number): HTMLTableRowElement {
            if (row >= this._rows) throw "No such row " + row + " (max " + (this._rows - 1) + ")";
            return <HTMLTableRowElement>this._table.rows[row];
        }

        /**
         * Get a direct reference to a header cell
         */
        public getHeaderCell(col: number, row: number): HTMLTableHeaderCellElement {
            if (col >= this._cols) throw "No such column " + col + " (max " + (this._cols - 1) + ")";
            return this._headerData[row][col];
        }

        /**
         * Get a direct reference to a footer cell
         */
        public getFooterCell(col: number): HTMLTableCellElement {
            if (col >= this._cols) throw "No such column " + col + " (max " + (this._cols - 1) + ")";
            return this._footerData[col];
        }

        /**
         * Remove a list of rows
         */
        public removeRows(rows: number[]): void {

            // Sort rows
            rows.sort((a, b) => {
                return a > b ? 1 : a < b ? -1 : 0;
            });

            // Remove duplicates
            {
                var a = rows[0];
                for (var i = 1, l = rows.length; i < l; ++i) {
                    if (rows[i] === a) {
                        rows.splice(i--, 1);
                        --l;
                        continue;
                    } else {
                        a = rows[i];
                    }
                }
            }
            
            console.log("Removing: ");
            console.log(rows);

            // Remove all rows
            for (var i = 0, l = this._rowData.length; i < l; ++i) {
                this._body.removeChild(this._rowData[i]);
            }

            // Remove row data from table
            for (var i = 0, l = rows.length; i < l; ++i) {
                this._rowData.splice(rows[i] - i, 1);
                this._cellData.splice(rows[i] - i, 1);
            }

            // Go through the sorted tdata array, appending rows back to body in now sorted order,
            // re-creating the cellData and roData arrays as we go along
            for (var i = 0, l = this._rowData.length; i < l; ++i) {

                var row = this._rowData[i];

                // Remove old style names
                util.removeStyleName('v-grid-row-even', row);
                util.removeStyleName('v-grid-row-odd', row);

                // Add style name back to maintain proper row coloring
                util.addStyleName(i & 1 ? 'v-grid-row-odd' : 'v-grid-row-even', row);

                // Add row back to body
                this._body.appendChild(row);
            }

        }

        /**
         * Sort all rows. Comparator receives complete row data to compare. Allows multi-column sorting.
         */
        public sort(comparator: (row_a: HTMLTableCellElement[], row_b: HTMLTableCellElement[]) => number): void {

            //
            // MergeSort, adapted from
            // http://en.wikibooks.org/wiki/Algorithm_Implementation/Sorting/Merge_sort
            //
            function mergeSort<T>(data: T[], comparator: (a: T, b: T) => number) {
                var i: number,
                    j: number,
                    k: number,
                    firstHalf: number,
                    secondHalf: number,
                    arr1: T[],
                    arr2: T[];

                if (data.length > 1) {
                    firstHalf = Math.floor(data.length / 2);
                    secondHalf = data.length - firstHalf;
                    arr1 = new Array<T>(firstHalf);
                    arr2 = new Array<T>(secondHalf);

                    for (i = 0; i < firstHalf; i++) {
                        arr1[i] = data[i];
                    }

                    for (i = firstHalf; i < firstHalf + secondHalf; i++) {
                        arr2[i - firstHalf] = data[i];
                    }

                    mergeSort(arr1, comparator);
                    mergeSort(arr2, comparator);

                    i = j = k = 0;

                    while (arr1.length != j && arr2.length != k) {
                        if (comparator(arr1[j], arr2[k]) <= 0) {
                            data[i] = arr1[j];
                            i++;
                            j++;
                        }
                        else {
                            data[i] = arr2[k];
                            i++;
                            k++;
                        }
                    }

                    while (arr1.length != j) {
                        data[i] = arr1[j];
                        i++;
                        j++;
                    }

                    while (arr2.length != k) {
                        data[i] = arr2[k];
                        i++;
                        k++;
                    }
                }
            }

            // Define temporary data structure
            var tdata: {
                origIdx: number;
                rowValue: HTMLTableRowElement;
                cells: HTMLTableCellElement[];
            }[] = [];

            for (var i = 0; i < this._rowData.length; ++i) {
                // Collect existing row/cell data
                tdata.push({
                    origIdx: i,
                    rowValue: this._rowData[i],
                    cells: this._cellData[i]
                });

                // Remove row from table
                this._body.removeChild(this._rowData[i]);

                // Remove any row order styling from row data
                util.removeStyleName('v-grid-row-even', this._rowData[i]);
                util.removeStyleName('v-grid-row-odd', this._rowData[i]);

            }

            // Use merge sort to sort through the data
            mergeSort(tdata, (ta, tb) => {
                return comparator(ta.cells, tb.cells);
            });

            // Clear the rowData array
            this._rowData = [];

            // Make a copy of existing cell data and clear that array too
            var celldata: HTMLTableCellElement[][] = this._cellData.splice(0, this._cellData.length);

            // Go through the sorted tdata array, appending rows back to body in now sorted order,
            // re-creating the cellData and roData arrays as we go along
            for (var i = 0, l = tdata.length; i < l; ++i) {

                // Re-create cellData and rowData arrays
                this._cellData.push(celldata[tdata[i].origIdx]);
                this._rowData.push(tdata[i].rowValue);

                // Add style name back to maintain proper row coloring
                util.addStyleName(i & 1 ? 'v-grid-row-odd' : 'v-grid-row-even', tdata[i].rowValue);

                // Add row back to body
                this._body.appendChild(tdata[i].rowValue);
            }

        }

        /**
         * Get access to the table wrapper element that can be used to place the table
         */
        public getElement() {
            return this._wrapper;
        }

        public focus(): void {
            this._table.focus();
        }
    }
}
