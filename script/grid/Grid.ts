/**
 * 
 * DANGER, WILL ROBINSON!
 * 
 * This code can be pretty fragile.
 * 
 */

// NOTICE: the auto-formatter for the typescript plugin is still a bit derpy

module grid {

    /**
     * Mock grid class implementation. Provides a friendly face for the coder.
     * Should handle the high-level gritty details.
     * 
     * The actual deep-level HTML structure is instead created by the Table
     * object; see Table.ts for that.
     * 
     * Please add common 
     */
    export class Grid {

        // Where is user navigating currently. (header,body,footer)
        public static NAVIGATION_HEADER: string = "header";
        public static NAVIGATION_BODY: string = "body";
        public static NAVIGATION_FOOTER: string = "footer";

        public static NAVIGATION_MODE_NAVIGATION: string = "navigation";
        public static NAVIGATION_MODE_ACTIONABLE: string = "actionable";

        private _pressedKeys: boolean[] = [];

        private _viewport: HTMLDivElement;
        private _table: Table;

        private _behaviors: Behavior[];

        private _columns: number;
        private _rows: number;

        private _viewportWidth: number;
        private _viewportHeight: number;

        private _selectedCells: HTMLTableCellElement[];

        private _focusedCell: HTMLTableCellElement;

        private _focusedBodyCellRow: number = 0;
        private _focusedBodyCellColumn: number = 1;

        private _focusedHeaderCellRow: number = 0;
        private _focusedHeaderCellColumn: number = 0;

        private _focusedFooterCellRow: number = 0;
        private _focusedFooterCellColumn: number = 0;

        private _navigationLocation: string = Grid.NAVIGATION_BODY;
        private _navigationMode: string = Grid.NAVIGATION_MODE_NAVIGATION;


        /**
         * Constructor handles basic setup and default value assignment.
         * Initializer methods and postInit take care of the rest.
         */
        public constructor(width: number = 600, height: number = 400) {

            // Store initial values
            this._viewportWidth = width;
            this._viewportHeight = height;
            this._behaviors = [];
            this._selectedCells = [];
            this._viewport = document.createElement("div");
            this._viewport.id = "v-grid-viewport";

        }

        /**
         * Initialize a Grid (and the underlying Table) from pre-created data arrays.
         */
        public initWithData(headerData: string[], data: string[][], footerData: string[]) {

            var rows: number, columns: number;

            if (headerData != null) {
                columns = headerData.length;
                console.log("Grid has header");
            }

            if (footerData != null) {
                if (columns === undefined) {
                    columns = footerData.length;
                } else {
                    columns = Math.max(headerData.length, footerData.length);
                }
                console.log("Grid has footer");
            }

            rows = data.length;

            this._columns = columns + 1;
            this._rows = rows;

            console.log("Initializing a grid with " + columns + " columns and " + rows + " rows based on input arrays");

            this._table = new Table(columns + 1, rows, (cell, x, y) => {

                // Create selection checkbox
                if (x == 0) {
                    cell.innerHTML = "<div class=\"checkbox\" style=\"background: url('imgs/check_deselected.png');\">";
                } else {

                    // Init with data
                    if (data != null && data[y] !== undefined) {
                        var input: string = data[y][x - 1];
                        if (input !== undefined) {

                            if (input.indexOf('#BOOL') === 0) {
                                var checked: number = parseInt(input.charAt(6));
                                if (checked == 0) {
                                    input = "<span class='inline-widget' style='text-align:center'><input type=\"checkbox\"></span>"
                                } else {
                                    input = "<span class='inline-widget' style='text-align:center'><input type=\"checkbox\" checked></span>"
                                }
                            } else if (input.indexOf("#OPTION") === 0) {
                                var selected: number = parseInt(input.charAt(8));
                                input = "<span>" + PersonnelGender[selected] + "</span>";

                            } else {
                                input = "<span>" + input + "</span>";
                            }

                            cell.innerHTML = input;
                        }
                    }
                }
            }, (cell, x) => {

                    // Create selection checkbox
                    if (x == 0) {
                        cell.innerHTML = "<div class=\"checkbox\" style=\"background: url('imgs/check_deselected.png');\">";
                    } else {

                        // Init with data
                        if (headerData != null) {
                            var input = headerData[x - 1];
                            if (input !== undefined) {
                                input = "<span>" + input + "</span><div class=\"extra\"></div>";
                                cell.innerHTML = input;
                            }
                        }
                    }
                }, (cell, x) => {

                    if (x == 0) {
                        // Reserved for selection column
                        cell.innerHTML = "<div>&nbsp;</div>";
                    } else {
                        if (footerData != null) {
                            var input = footerData[x - 1];
                            if (input !== undefined) {
                                input = "<span>" + input + "</span><div class=\"extra\"></div>";
                                cell.innerHTML = input;
                            }
                        }
                    }
                });

            this.setColumnSortable(0, false);

            this.postInit();
        }

        /**
         * Create bogus data arrays and create a table from those
         */
        public initWithBogusData(columns: number, rows: number) {

            var headerData = new Array<string>(columns);
            var footerData = new Array<string>(columns);
            var bodyData = new Array<Array<string>>(rows);

            for (var i = 0; i < rows; ++i) {
                var ary = bodyData[i] = new Array<string>(columns);

                for (var x = 0; x < columns; ++x) {
                    ary[x] = "" + ((Math.random() * 25) | 0);
                }
            }

            for (var i = 0; i < columns; ++i) {
                headerData[i] = "Header " + (i + 1);
                footerData[i] = "Footer " + (i + 1);
            }

            this.initWithData(headerData, bodyData, footerData);

        }

        /**
         * Hook up all necessary stuff (events, DOM, etc) to the Grid structure 
         */
        private postInit() {

            //
            // Attach event handling to table
            //

            // Standard click event
            this._table.addClickListener((target, cell, col, row, evt) => {
                switch (target) {
                    case EventDestination.HEADER:
                        this._behaviors.forEach((behavior) => {
                            if (this._navigationMode == Grid.NAVIGATION_MODE_ACTIONABLE) {
                                if (behavior.activeInActionableMode()) {
                                    behavior.headerClicked(this, cell, col, row, evt);
                                }
                            } else {
                                behavior.headerClicked(this, cell, col, row, evt);
                            }
                        });
                        break;

                    case EventDestination.FOOTER:
                        this._behaviors.forEach((behavior) => {
                            if (this._navigationMode == Grid.NAVIGATION_MODE_ACTIONABLE) {
                                if (behavior.activeInActionableMode()) {
                                    behavior.footerClicked(this, cell, col, evt);
                                }
                            } else {
                                behavior.footerClicked(this, cell, col, evt);
                            }
                        });
                        break;

                    case EventDestination.BODY:
                        this._behaviors.forEach((behavior) => {
                            if (this._navigationMode == Grid.NAVIGATION_MODE_ACTIONABLE) {
                                if (behavior.activeInActionableMode()) {
                                    behavior.cellClicked(this, cell, col, row, evt);
                                }
                            } else {
                                behavior.cellClicked(this, cell, col, row, evt);
                            }
                        });
                        break;
                }
            });

            // Low-level mouse down event
            this._table.addMouseDownListener((evt) => {
                this._behaviors.forEach((behavior) => {
                    if (this._navigationMode == Grid.NAVIGATION_MODE_ACTIONABLE) {
                        if (behavior.activeInActionableMode()) {
                            behavior.mouseDownEvent(this, evt);
                        }
                    } else {
                        behavior.mouseDownEvent(this, evt);
                    }
                });
            });

            // Low-level mouse up event
            this._table.addMouseUpListener((evt) => {
                this._behaviors.forEach((behavior) => {
                    if (this._navigationMode == Grid.NAVIGATION_MODE_ACTIONABLE) {
                        if (behavior.activeInActionableMode()) {
                            behavior.mouseUpEvent(this, evt);
                        }
                    } else {
                        behavior.mouseUpEvent(this, evt);
                    }

                });
            });

            // Low-level mouse move event
            this._table.addMouseMoveListener((evt) => {
                this._behaviors.forEach((behavior) => {
                    if (this._navigationMode == Grid.NAVIGATION_MODE_ACTIONABLE) {
                        if (behavior.activeInActionableMode()) {
                            behavior.mouseMoveEvent(this, evt);
                        }
                    } else {
                        behavior.mouseMoveEvent(this, evt);
                    }
                });
            });

            // Low-level touch start event
            this._table.addTouchStartListener((evt) => {
                this._behaviors.forEach((behavior) => {
                    if (this._navigationMode == Grid.NAVIGATION_MODE_ACTIONABLE) {
                        if (behavior.activeInActionableMode()) {
                            behavior.touchStartEvent(this, evt);
                        }
                    } else {
                        behavior.touchStartEvent(this, evt);
                    }
                });
            });

            // Low-level touch move event
            this._table.addTouchMoveListener((evt) => {
                this._behaviors.forEach((behavior) => {
                    if (this._navigationMode == Grid.NAVIGATION_MODE_ACTIONABLE) {
                        if (behavior.activeInActionableMode()) {
                            behavior.touchMoveEvent(this, evt);
                        }
                    } else {
                        behavior.touchMoveEvent(this, evt);
                    }
                });
            });

            // Low-level touch end event
            this._table.addTouchEndListener((evt) => {
                this._behaviors.forEach((behavior) => {
                    if (this._navigationMode == Grid.NAVIGATION_MODE_ACTIONABLE) {
                        if (behavior.activeInActionableMode()) {
                            behavior.touchEndEvent(this, evt);
                        }
                    } else {
                        behavior.touchEndEvent(this, evt);
                    }
                });
            });


            //
            // Add keyboard event dispatcher
            //

            // Really raw down-to-the-HTML event listener
            this._table.addKeyboardListener((evt) => {
                this._behaviors.forEach((behavior) => {

                    // Retrieve the currently focused cell
                    var key = evt.keyCode;
                    var type = evt.type;
                    var cell = this.getFocusedCell();
                    var colidx = this.getFocusedColumnIndex();
                    var rowidx = this.getFocusedRowIndex();

                    // Figure out what method to call
                    switch (this._navigationLocation) {
                        case Grid.NAVIGATION_HEADER: {
                            this._behaviors.forEach((behavior) => {
                                switch (type) {
                                    case 'keyup':
                                        if (this._navigationMode == Grid.NAVIGATION_MODE_ACTIONABLE) {
                                            if (behavior.activeInActionableMode()) {
                                                behavior.headerKeyUp(this, cell, key, colidx, rowidx, evt);
                                            }
                                        } else {
                                            behavior.headerKeyUp(this, cell, key, colidx, rowidx, evt);
                                        }

                                        break;
                                    case 'keydown':

                                        if (this._navigationMode == Grid.NAVIGATION_MODE_ACTIONABLE) {
                                            if (behavior.activeInActionableMode()) {
                                                behavior.headerKeyDown(this, cell, key, colidx, rowidx, evt);
                                                if (!this._pressedKeys[key]) {
                                                    behavior.headerKeyPressed(this, cell, key, colidx, rowidx, evt);
                                                }
                                            }
                                        } else {
                                            behavior.headerKeyDown(this, cell, key, colidx, rowidx, evt);
                                            if (!this._pressedKeys[key]) {
                                                behavior.headerKeyPressed(this, cell, key, colidx, rowidx, evt);
                                            }
                                        }

                                        break;
                                    case 'keypress':
                                        if (this._navigationMode == Grid.NAVIGATION_MODE_ACTIONABLE) {
                                            if (behavior.activeInActionableMode()) {
                                                behavior.headerKeyPressed(this, cell, key, colidx, rowidx, evt);
                                            }
                                        } else {
                                            behavior.headerKeyPressed(this, cell, key, colidx, rowidx, evt);
                                        }

                                        break;
                                    default:
                                        console.log("Unhandled keyboard event type " + type + " in header");
                                        break;
                                }
                            });
                        } break;
                        case Grid.NAVIGATION_BODY: {
                            this._behaviors.forEach((behavior) => {
                                switch (type) {
                                    case 'keyup':
                                        if (this._navigationMode == Grid.NAVIGATION_MODE_ACTIONABLE) {
                                            if (behavior.activeInActionableMode()) {
                                                behavior.cellKeyUp(this, cell, key, colidx, rowidx, evt);
                                            }
                                        } else {
                                            behavior.cellKeyUp(this, cell, key, colidx, rowidx, evt);
                                        }

                                        break;
                                    case 'keydown':
                                        if (this._navigationMode == Grid.NAVIGATION_MODE_ACTIONABLE) {
                                            if (behavior.activeInActionableMode()) {
                                                behavior.cellKeyDown(this, cell, key, colidx, rowidx, evt);
                                                if (!this._pressedKeys[key]) {
                                                    behavior.cellKeyPressed(this, cell, key, colidx, rowidx, evt);
                                                }
                                            }
                                        } else {
                                            behavior.cellKeyDown(this, cell, key, colidx, rowidx, evt);
                                            if (!this._pressedKeys[key]) {
                                                behavior.cellKeyPressed(this, cell, key, colidx, rowidx, evt);
                                            }
                                        }

                                        break;
                                    case 'keypress':
                                        if (this._navigationMode == Grid.NAVIGATION_MODE_ACTIONABLE) {
                                            if (behavior.activeInActionableMode()) {
                                                behavior.cellKeyPressed(this, cell, key, colidx, rowidx, evt);
                                            }
                                        } else {
                                            behavior.cellKeyPressed(this, cell, key, colidx, rowidx, evt);
                                        }

                                        break;
                                    default:
                                        console.log("Unhandled keyboard event type " + type + " in body");
                                        break;

                                }
                            });
                        } break;
                        case Grid.NAVIGATION_FOOTER: {
                            this._behaviors.forEach((behavior) => {
                                switch (type) {
                                    case 'keyup':
                                        if (this._navigationMode == Grid.NAVIGATION_MODE_ACTIONABLE) {
                                            if (behavior.activeInActionableMode()) {
                                                behavior.footerKeyUp(this, cell, key, colidx, evt);
                                            }
                                        } else {
                                            behavior.footerKeyUp(this, cell, key, colidx, evt);
                                        }

                                        break;
                                    case 'keydown':
                                        if (this._navigationMode == Grid.NAVIGATION_MODE_ACTIONABLE) {
                                            if (behavior.activeInActionableMode()) {
                                                behavior.footerKeyDown(this, cell, key, colidx, evt);
                                                if (!this._pressedKeys[key]) {
                                                    behavior.footerKeyPressed(this, cell, key, colidx, evt);
                                                }
                                            }
                                        } else {
                                            behavior.footerKeyDown(this, cell, key, colidx, evt);
                                            if (!this._pressedKeys[key]) {
                                                behavior.footerKeyPressed(this, cell, key, colidx, evt);
                                            }
                                        }

                                        break;
                                    case 'keypress':
                                        if (this._navigationMode == Grid.NAVIGATION_MODE_ACTIONABLE) {
                                            if (behavior.activeInActionableMode()) {
                                                behavior.footerKeyPressed(this, cell, key, colidx, evt);
                                            }
                                        } else {
                                            behavior.footerKeyPressed(this, cell, key, colidx, evt);
                                        }
                                        break;
                                    default:
                                        console.log("Unhandled keyboard event type " + type + " in footer");
                                        break;

                                }
                            });
                        } break;
                    }

                    switch (evt.type) {
                        case 'keyup':
                            this._pressedKeys[key] = false;
                            break;
                        case 'keydown':
                            this._pressedKeys[key] = true;
                            break;
                    }

                    // Call old-skool keyboard listener
                    if (this._navigationMode == Grid.NAVIGATION_MODE_ACTIONABLE) {
                        if (behavior.activeInActionableMode()) {
                            behavior.keyboardEvent(this, evt);
                        }
                    } else {
                        behavior.keyboardEvent(this, evt);
                    }
                });
            });


            //
            // Focus and blur event dispatchers
            //

            this._table.addFocusListener((evt) => {
                this._behaviors.forEach((behavior) => {

                    // Ensure previous focus
                    if (this._focusedCell != null) {
                        util.removeStyleName('focused-inactive', this._focusedCell);
                        util.addStyleName('focused', this._focusedCell);
                    } else {
                        //
                        // This possibly unnecessary line caused focus to jump on initial focus.
                        //
                        //this.focusCell(this._focusedBodyCellColumn, this._focusedBodyCellRow);
                    }

                    behavior.focused(this, evt);
                });
            });

            this._table.addBlurListener((evt) => {
                this._behaviors.forEach((behavior) => {

                    // Loose focus when moving out of cell
                    if (this._focusedCell != null) {
                        util.removeStyleName('focused', this._focusedCell);
                        util.addStyleName('focused-inactive', this._focusedCell);
                    }

                    behavior.unfocused(this, evt);
                });
            });

            // Append table to viewport
            this._viewport.appendChild(this._table.getElement());
        }

        /**
         * Remove all selected rows
         */
        public removeSelectedRows() {
            var rows: number[] = [];

            // Duplicates are taken care of on the table side
            this._selectedCells.forEach((cell) => {
                rows.push(this._table.getCellCoordinates(cell).y);
            });

            this._table.removeRows(rows);
            this.clearSelectedCells();
            
            if(!this.getLocationDependentCoordinatesFor(this._focusedCell)) {
                this._focusedCell = null;
                this._behaviors.forEach((b) => {
                   b.focusChanged(this,null,-1,-1);
                });
            }
        }

        /**
         * See if a certain column is marked as sortable.
         */
        public isColumnSortable(column: number) {
            return this._table.isColumnSortable(column);
        }

        /**
         * Toggle sortability of a column
         */
        public setColumnSortable(column: number, value: boolean) {
            this._table.setColumnSortable(column, value);
        }

        /**
         * Get direct access to a table cell
         */
        public getCell(x: number, y: number) {
            return this._table.getCell(x, y);
        }

        /**
         * Get direct access to a header cell
         */
        public getHeaderCell(col: number, row: number) {
            try {
                return this._table.getHeaderCell(col, row);
            } catch (e) {
                return null;
            }
        }

        /**
         * Get extra data area for a header cell (if it exists).
         * Used e.g. to indicate sort order.
         */
        public getHeaderCellExtra(col: number, row: number): HTMLElement {
            try {
                return <HTMLElement>(this._table.getHeaderCell(col, row).getElementsByClassName("extra")[0]);
            } catch (e) {
                return null;
            }
        }

        /**
         * Get direct access to a footer cell
         */
        public getFooterCell(col: number) {
            return this._table.getFooterCell(col);
        }

        /**
         * Add a style name to a table cell
         */
        public addCellStyle(x: number, y: number, styleName: string) {
            util.addStyleName(styleName, this.getCell(x, y));
        }

        /**
         * Remove a style name from a table cell
         */
        public removeCellStyle(x: number, y: number, styleName: string) {
            util.removeStyleName(styleName, this.getCell(x, y));
        }

        /**
         * Mark a cell as selected, if it is not currently selected
         */
        public selectCell(x: number, y: number) {
            if (!this.isCellSelected(x, y)) {
                var cell = this.getCell(x, y);
                util.addStyleName('v-grid-cell-selected', cell);
                this._selectedCells.push(cell);

                this._behaviors.forEach((b) => {
                    b.selectionChanged(this,this._selectedCells);
                });
            }

            document.getElementById('selection-controls').style.display = 'block';
        }

        /**
         * Mark a cell as not selected, if it is currently selected
         */
        public unselectCell(x: number, y: number) {
            if (this.isCellSelected(x, y)) {
                var cell = this.getCell(x, y);
                util.removeStyleName('v-grid-cell-selected', cell);
                this._selectedCells.splice(this._selectedCells.indexOf(cell), 1);

                this._behaviors.forEach((b) => {
                    b.selectionChanged(this,this._selectedCells);
                });
            }

            if (!this._selectedCells.length) {
                document.getElementById('selection-controls').style.display = 'none';
            }
        }

        /**
         * Test if a cell is currently marked as selected
         */
        public isCellSelected(x: number, y: number) {
            return this._selectedCells.indexOf(this.getCell(x, y)) > -1;
        }

        /**
         * Get all currently selected cells. Returns a copy of an internal array.
         */
        public getSelectedCells() {
            return this._selectedCells.slice(0);
        }

        /**
         * Remove selection from all currently selected cells
         */
        public clearSelectedCells() {
            for (var i = 0, l = this._selectedCells.length; i < l; ++i) {
                util.removeStyleName('v-grid-cell-selected', this._selectedCells[i]);
            }
            this._selectedCells.splice(0, this._selectedCells.length);
            document.getElementById('selection-controls').style.display = 'none';
        }

        /**
         * Focuses a cell
         */
        public focusCell(x: number, y: number) {

            console.log("focusing cell " + x + "," + y);

            var firstFocus: boolean = this._focusedCell == null;

            if (this._focusedCell != null) {
                // Unfocus previous cell
                var coords: number[] = this.getLocationDependentCoordinatesFor(this._focusedCell);
                if (coords != null) {
                    util.removeStyleName('focused-inactive', this._focusedCell);
                    util.removeStyleName('focused', this._focusedCell);
                    util.removeStyleName('focused-row', this._focusedCell.parentElement);
                    util.removeStyleName('focused-column', this.getHeaderCell(coords[1], this.getHeaderCount() - 1));
                }
                this._focusedCell = null;
            }

            if (this._navigationLocation == Grid.NAVIGATION_BODY) {
                if (x >= 0 && x < this.getColumnCount() && y >= 0 && y < this.getRowCount()) {
                    // Focus new cell
                    this._focusedCell = this.getCell(x, y);
                    if (this._focusedCell != null) {
                        this._focusedBodyCellRow = y;
                        this._focusedBodyCellColumn = x;
                        util.addStyleName('focused', this._focusedCell);
                        util.addStyleName('focused-row', this._focusedCell.parentElement);
                        util.addStyleName('focused-column', this.getHeaderCell(x, this.getHeaderCount() - 1));

                        // Scroll into view if necessary   
                        var tbody: HTMLElement = <HTMLElement> this._table.getElement().getElementsByTagName('tbody').item(0);
                        var scrollPosition: number = tbody.offsetHeight + tbody.scrollTop
                            - this._focusedCell.parentElement.offsetTop
                            - this._focusedCell.offsetHeight;

                        ///
                        /// HERE BE DRAGONS
                        ///
                        
                        if (scrollPosition < 0) {
                            // Bottom edge
                            console.log("bark " + scrollPosition);
                            this._focusedCell.scrollIntoView(false);
                        } else if (scrollPosition > tbody.offsetHeight - this._focusedCell.offsetHeight) {
                            //Top edge
                            console.log("w00f" + scrollPosition);
                            this._focusedCell.scrollIntoView(true);
                        }

                    }
                }

            } else if (this._navigationLocation == Grid.NAVIGATION_FOOTER) {

                this._focusedCell = this.getFooterCell(x);
                if (this._focusedCell == null) return;

                this._focusedFooterCellRow = y;
                this._focusedFooterCellColumn = x;
                util.addStyleName('focused', this._focusedCell);
                util.addStyleName('focused-row', this._focusedCell.parentElement);
                this._focusedCell.scrollIntoView(true);

            } else if (this._navigationLocation == Grid.NAVIGATION_HEADER) {

                this._focusedCell = this.getHeaderCell(x, y);
                if (this._focusedCell == null) return;

                this._focusedHeaderCellRow = y;
                this._focusedHeaderCellColumn = x;
                util.addStyleName('focused', this._focusedCell);
                util.addStyleName('focused-row', this._focusedCell.parentElement);
                this._focusedCell.scrollIntoView(false);
            }

            // Notify behaviors
            this._behaviors.forEach((b) => {
                b.focusChanged(this, this._focusedCell, x, y);
            });
        }

        /**
         * Returns the currently focused cell
         */
        public getFocusedCell() {
            return this._focusedCell;
        }

        /**
         * Returns the index of the column that is focused
         */
        public getFocusedColumnIndex(): number {
            switch (this._navigationLocation) {
                case Grid.NAVIGATION_HEADER: return this._focusedHeaderCellColumn;
                case Grid.NAVIGATION_BODY: return this._focusedBodyCellColumn;
                case Grid.NAVIGATION_FOOTER: return this._focusedFooterCellColumn;
                default: return 0;
            }
        }

        /**
         * Returns the index of the row the is focused
         */
        public getFocusedRowIndex(): number {
            switch (this._navigationLocation) {
                case Grid.NAVIGATION_HEADER: return this._focusedHeaderCellRow;
                case Grid.NAVIGATION_BODY: return this._focusedBodyCellRow;
                case Grid.NAVIGATION_FOOTER: return this._focusedFooterCellRow;
                default: return 0;
            }
        }

        /**
         * Sets the position where the navigation currently is. 
         * 
         * Possible values are:  
         *  NAVIGATION_HEADER, 
         *  NAVIGATION_BODY,
         *  NAVIGATION_FOOTER 
         */
        public setNavigationLocation(location: string): void {
            this._navigationLocation = location;
            console.log("Navigation in " + location);

            var x: number = 0;
            var y: number = 0;
            switch (this._navigationLocation) {
                case Grid.NAVIGATION_HEADER:
                    x = this._focusedHeaderCellColumn;
                    y = this._focusedHeaderCellRow;
                    break;
                case Grid.NAVIGATION_BODY:
                    x = this._focusedBodyCellColumn;
                    y = this._focusedBodyCellRow;
                    break;
                case Grid.NAVIGATION_FOOTER:
                    x = this._focusedFooterCellColumn;
                    y = this._focusedFooterCellRow;
                    break;
            }
        }

        /**
         * Returns the navigation location
         */
        public getNavigationLocation(): string {
            return this._navigationLocation;
        }

        /**
         * Sets the current navigation mode
         */
        public setNavigationMode(mode: string): void {
            this._navigationMode = mode;
            console.log("Navigation mode is now " + mode);
        }

        /**
         * Returns the navigation mode
         */
        public getNavigationMode(): string {
            return this._navigationMode;
        }

        /**
         * Sort table by column
         */
        public sortByColumn(column: number, comparator: (a: HTMLTableCellElement, b: HTMLTableCellElement) => number): void {
            this._table.sort((ra, rb) => {
                return comparator(ra[column], rb[column]);
            });
        }

        /**
         * Sort by complete row data
         */
        public sortByRow(comparator: (row_a: HTMLTableCellElement[], b: HTMLTableCellElement[]) => number): void {
            this._table.sort(comparator);
        }

        /**
         * Add a behavior handler
         */
        public addBehavior(b: Behavior): void {
            var idx = this._behaviors.indexOf(b);
            if (idx < 0) {
                this._behaviors.push(b);
            }
        }

        /**
         * Remove a behavior handler
         */
        public removeBehavior(b: Behavior): void {
            var idx = this._behaviors.indexOf(b);
            if (idx >= 0) {
                this._behaviors.splice(idx, 1);
            }
        }

        /**
         * Get current viewport width in pixels
         */
        public getViewportWidth(): number {
            return this._viewportWidth;
        }

        /**
         * Get current viewport height in pixels
         */
        public getViewportHeight(): number {
            return this._viewportHeight;
        }

        /**
         * Force viewport size in pixels
         */
        public setViewportSize(width: number, height: number): void {
            this._viewportWidth = width;
            this._viewportHeight = height;
            // this._viewport.style.width = width + "px";
            // this._viewport.style.height = height + "px";
        }

        /**
         * Return the number of columns
         */
        public getRowCount(): number {
            return this._rows;
        }

        /**
         * Return the number of rows
         */
        public getColumnCount(): number {
            return this._columns;
        }

        public getHeaderCount(): number {
            return 1; //Constant
        }

        /**
         * Get an access to the DOM element containing the Grid component.
         */
        public getElement(): Element {
            return this._viewport;
        }

        /**
         * Get direct access to the Table object that provides the actual data view
         */
        public getTable(): Table {
            return this._table;
        }

        public getLocationDependentCoordinatesFor(element: Element): number[] {
            var location: string = this.getLocationOf(element);
            var coords: number[] = null;
            if (location == grid.Grid.NAVIGATION_HEADER) {
                coords = this.getBodyCoordinatesFor(element, 'TH');
            } else if (location == grid.Grid.NAVIGATION_BODY) {
                coords = this.getBodyCoordinatesFor(element);
            } else if (location == grid.Grid.NAVIGATION_FOOTER) {
                coords = this.getBodyCoordinatesFor(element);
            }
            return coords;
        }

        /**
         * Extract cell coordinates using DOM magic.
         * TODO: could someone PLEASE document this logic?
         */
        public getBodyCoordinatesFor(element: Element, tagName: string = "TD"): number[] {
            while (element != null && element.tagName != tagName) {
                element = <Element> element.parentNode;
            }

            if (element != null) {
                var col = -1;
                var cellCursor = element;
                while (cellCursor != null) {
                    cellCursor = cellCursor.previousElementSibling;
                    col++;
                }

                var row = -1;
                var rowCursor = <Element> element.parentNode;
                while (rowCursor != null) {
                    rowCursor = rowCursor.previousElementSibling;
                    row++;
                }

                return [row, col];
            }

            return null;
        }

        public getLocationOf(element: Node): string {
            while (element != null && element.nodeName != "TABLE") {
                element = element.parentNode;
                if (element == null) return null;
                switch (element.nodeName) {
                    case "TBODY": return Grid.NAVIGATION_BODY; break;
                    case "THEAD": return Grid.NAVIGATION_HEADER; break;
                    case "TFOOT": return Grid.NAVIGATION_FOOTER; break;
                }
            }

            return null;
        }


    }

}
