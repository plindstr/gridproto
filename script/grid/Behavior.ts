/*
 * DANGER, WILL ROBINSON!
 * 
 * This code can be pretty fragile.
 */

module grid {

    /**
     * Grid element behavior modifier.
     * All behavior handlers receive all Grid events,
     * but each handler only needs to modify a single aspect.
     * 
     * Instead of having to listen to individual events, just
     * override whatever methods you need. Should be self-explanatory.
     */
    export class Behavior {


        //================
        // Mouse behavior 
        //================


        /**
         * Called when a cell in the main table body is clicked
         */
        public cellClicked(grid: Grid, cell: HTMLElement, column: number, row: number, event: MouseEvent) {

        }

        /**
         * Called when a cell in a header is clicked
         */
        public headerClicked(grid: Grid, cell: HTMLElement, column: number, row: number, event: MouseEvent) {

        }

        /**
         * Called when a cell in a footer is clicked
         */
        public footerClicked(grid: Grid, cell: HTMLElement, column: number, event: MouseEvent) {

        }


        //==========================
        // Low-level mouse behavior
        //==========================


        /**
         * Called when the body gets a mousedown event
         */
        public mouseDownEvent(grid: Grid, event: MouseEvent) {

        }

        /**
         * Called when the body gets a mouseup event
         */
        public mouseUpEvent(grid: Grid, event: MouseEvent) {

        }

        /**
         * Called when the body gets a mousemove event
         */
        public mouseMoveEvent(grid: Grid, event: MouseEvent) {

        }



        //=============================
        // New-style keyboard behavior
        //=============================


        /**
         * Called ONCE for each time a key is pressed, while the target of the keyboard event is the header
         */
        public headerKeyPressed(grid: Grid, cell: HTMLElement, keyCode: number, column: number, row: number, event: KeyboardEvent) {

        }

        /**
         * Called every time the header receives a keydown event
         */
        public headerKeyDown(grid: Grid, cell: HTMLElement, keyCode: number, column: number, row: number, event: KeyboardEvent) {

        }

        /**
         * Called every time the header receives a keyup event
         */
        public headerKeyUp(grid: Grid, cell: HTMLElement, keyCode: number, column: number, row: number, event: KeyboardEvent) {

        }



        /**
         * Called ONCE for each time a key is pressed, when the target of the keyboard event is the footer
         */
        public footerKeyPressed(grid: Grid, cell: HTMLElement, keyCode: number, column: number, event: KeyboardEvent) {

        }

        /**
         * Called every time the footer receives a keydown event
         */
        public footerKeyDown(grid: Grid, cell: HTMLElement, keyCode: number, column: number, event: KeyboardEvent) {

        }

        /**
         * Called every time the footer receives a keyup event
         */
        public footerKeyUp(grid: Grid, cell: HTMLElement, keyCode: number, column: number, event: KeyboardEvent) {

        }



        /**
         * Called ONCE for each time a key is pressed, when the target of the keyboard event is a cell in the table
         */
        public cellKeyPressed(grid: Grid, cell: HTMLElement, keyCode: number, column: number, row: number, event: KeyboardEvent) {

        }

        /**
         * Called every time the table body receives a keydown event
         */
        public cellKeyDown(grid: Grid, cell: HTMLElement, keyCode: number, column: number, row: number, event: KeyboardEvent) {

        }

        /**
         * Called every time the table body receives a keydown event
         */
        public cellKeyUp(grid: Grid, cell: HTMLElement, keyCode: number, column: number, row: number, event: KeyboardEvent) {

        }


        //=============================
        // Old-style keyboard behavior
        //=============================

        /**
         * Called when a cell in the body gets a keyboard event
         */
        public keyboardEvent(grid: Grid, event: KeyboardEvent) {

        }


        //================
        // Focus behavior
        //================

        /**
         * Triggered when the Grid recieved focus
         */
        public focused(grid: Grid, event: FocusEvent) {

        }

        /**
         * Called when the Grid looses focus
         */
        public unfocused(grid: Grid, event: FocusEvent) {

        }

        //================
        // Touch behavior
        //================

        public touchStartEvent(grid: Grid, event: TouchEvent) {

        }

        public touchMoveEvent(grid: Grid, event: TouchEvent) {

        }

        public touchEndEvent(grid: Grid, event: TouchEvent) {

        }

        /**
         * Should behaviour be active while grid is in actionable mode?
         */
        public activeInActionableMode(): boolean {
            return false;
        }

        /**
         * Called when selection changes
         */
        public selectionChanged(grid: Grid, selectedCells: HTMLTableCellElement[]): void {

        }

        /**
         * Called when focus is set
         */
        public focusChanged(grid: Grid, cell: HTMLElement, column: number, row: number) {

        }

    }

}
