/**
 * Handles navigation when the grid is in navigation mode
 */
class NavigationModeBehavior extends grid.Behavior {

    /**
     * Called when a cell in the body gets a keyboard event
     */
    public keyboardEvent(gridImpl: grid.Grid, event: KeyboardEvent): void {    
        if(gridImpl.getNavigationMode() != grid.Grid.NAVIGATION_MODE_NAVIGATION){
            return;
        }
        
        if (event.type === 'keydown') {
            this.handleKeyDown(gridImpl, event);
        }
    }

    /**
     * Called when the body gets a mousedown event
     */
    public mouseDownEvent(gridImpl: grid.Grid, event: MouseEvent) {
        if(gridImpl.getNavigationMode() != grid.Grid.NAVIGATION_MODE_NAVIGATION){
            return;
        }
        
        var coords : number[] = gridImpl.getLocationDependentCoordinatesFor(event.toElement);
        if (coords == null) return;

        var row = coords[0];
        var column = coords[1];
        
        gridImpl.getTable().focus();
        gridImpl.setNavigationLocation(gridImpl.getLocationOf(event.toElement));
        gridImpl.focusCell(column, row);
    }

    /**
     * Handles any key down
     */
    private handleKeyDown(grid: grid.Grid, event: KeyboardEvent): void {
        switch (event.keyCode) {
            case KeyCodes.ARROW_UP: this.handleArrowUp(grid, event); break;
            case KeyCodes.ARROW_DOWN: this.handleArrowDown(grid, event); break;
            case KeyCodes.ARROW_LEFT: this.handleArrowLeft(grid, event); break;
            case KeyCodes.ARROW_RIGHT: this.handleArrowRight(grid, event); break;
            case KeyCodes.TAB: this.handleTabKey(grid, event); break;
            case KeyCodes.PG_UP: this.handlePgUp(grid, event); break;
            case KeyCodes.PG_DOWN: this.handlePgDown(grid, event); break;
            case KeyCodes.HOME: this.handleHome(grid, event); break;
            case KeyCodes.END: this.handleEnd(grid, event); break;
        }
    }

    /**
     * Handles pressing the arrow down key in navigation mode
     */
    private handleArrowDown(gridImpl: grid.Grid, event: KeyboardEvent): void {
        if (gridImpl.getNavigationLocation() == grid.Grid.NAVIGATION_BODY) {
            if (gridImpl.getFocusedRowIndex() < gridImpl.getRowCount() - 1) {
                gridImpl.focusCell(gridImpl.getFocusedColumnIndex(), gridImpl.getFocusedRowIndex() + 1);
            } else if(gridImpl.getFocusedRowIndex() == gridImpl.getRowCount() -1){
                var column : number = gridImpl.getFocusedColumnIndex();
                gridImpl.setNavigationLocation(grid.Grid.NAVIGATION_FOOTER);
                gridImpl.focusCell(column, 0);   
            }
        } else if (gridImpl.getNavigationLocation() == grid.Grid.NAVIGATION_HEADER) {
            if (gridImpl.getFocusedRowIndex() < gridImpl.getHeaderCount() - 1) {
                gridImpl.focusCell(gridImpl.getFocusedColumnIndex(), gridImpl.getFocusedRowIndex() + 1);          
            } else if(gridImpl.getFocusedRowIndex() == gridImpl.getHeaderCount() -1){
                var column : number = gridImpl.getFocusedColumnIndex();
                gridImpl.setNavigationLocation(grid.Grid.NAVIGATION_BODY);
                gridImpl.focusCell(column, 0);       
            }
        } 
        event.preventDefault();
    }
    
    /**
     * Handles pressing the arrow up key in navigation mode 
     */
    private handleArrowUp(gridImpl: grid.Grid, event: KeyboardEvent): void {
        if (gridImpl.getNavigationLocation() == grid.Grid.NAVIGATION_BODY) {
            if (gridImpl.getFocusedRowIndex() > 0) {
                gridImpl.focusCell(gridImpl.getFocusedColumnIndex(), gridImpl.getFocusedRowIndex() - 1);
            } else if(gridImpl.getFocusedRowIndex() == 0){
                var column : number = gridImpl.getFocusedColumnIndex();
                gridImpl.setNavigationLocation(grid.Grid.NAVIGATION_HEADER);
                gridImpl.focusCell(column, gridImpl.getHeaderCount()-1);   
            }
        } else if (gridImpl.getNavigationLocation() == grid.Grid.NAVIGATION_HEADER) {
            if (gridImpl.getFocusedRowIndex() > 0) {
                gridImpl.focusCell(gridImpl.getFocusedColumnIndex(), gridImpl.getFocusedRowIndex() - 1);
            }
        } else if (gridImpl.getNavigationLocation() == grid.Grid.NAVIGATION_FOOTER) {
            if(gridImpl.getFocusedRowIndex() == 0){
                var column : number = gridImpl.getFocusedColumnIndex();
                gridImpl.setNavigationLocation(grid.Grid.NAVIGATION_BODY);
                gridImpl.focusCell(column, gridImpl.getRowCount()-1);       
            }
        }
        event.preventDefault();
    }

    /*
     * Handles pressing left arrow
     */
    private handleArrowLeft(grid: grid.Grid, event: KeyboardEvent): void {
        if (grid.getFocusedColumnIndex() > 0) {
            grid.focusCell(grid.getFocusedColumnIndex() - 1, grid.getFocusedRowIndex());
            event.preventDefault();
        }
    }

    /**
     * Handles pressing right arrow 
     */
    private handleArrowRight(grid: grid.Grid, event: KeyboardEvent): void {
        if (grid.getFocusedColumnIndex() < grid.getColumnCount() - 1) {
            grid.focusCell(grid.getFocusedColumnIndex() + 1, grid.getFocusedRowIndex());
            event.preventDefault();
        }
    }
    
    private handlePgUp(grid: grid.Grid, event: KeyboardEvent): void {
        var row : number = grid.getFocusedRowIndex() - 21;
        if(row < 0) row = 0;
        grid.focusCell(grid.getFocusedColumnIndex(), row);   
        event.preventDefault();  
    }
    
    private handlePgDown(grid: grid.Grid, event: KeyboardEvent): void {
        var row : number = grid.getFocusedRowIndex() + 21;
        if(row >= grid.getRowCount()) row = grid.getRowCount() -1;
        grid.focusCell(grid.getFocusedColumnIndex(), row);   
        event.preventDefault();  
    }
    
    private handleHome(grid: grid.Grid, event: KeyboardEvent): void {
        grid.focusCell(grid.getFocusedColumnIndex(), 0);   
        event.preventDefault();  
    }
    
    private handleEnd(grid: grid.Grid, event: KeyboardEvent): void {
        grid.focusCell(grid.getFocusedColumnIndex(), grid.getRowCount()-1);   
        event.preventDefault();  
    }

    /**
     * Handles pressing tab
     */
    private handleTabKey(gridImpl: grid.Grid, event: KeyboardEvent): void {
        var currentColumnIndex : number = gridImpl.getFocusedColumnIndex();
        if (event.shiftKey) {
            switch (gridImpl.getNavigationLocation()) {
                case grid.Grid.NAVIGATION_BODY:                    
                    gridImpl.setNavigationLocation(grid.Grid.NAVIGATION_HEADER);
                    event.preventDefault();
                    gridImpl.focusCell(currentColumnIndex, gridImpl.getFocusedRowIndex());
                    break;
                case grid.Grid.NAVIGATION_FOOTER:
                    gridImpl.setNavigationLocation(grid.Grid.NAVIGATION_BODY);
                    event.preventDefault();
                    gridImpl.focusCell(currentColumnIndex, gridImpl.getFocusedRowIndex());
                    break;
                case grid.Grid.NAVIGATION_HEADER:
                    break;
            }
        } else {
            switch (gridImpl.getNavigationLocation()) {
                case grid.Grid.NAVIGATION_BODY:
                    gridImpl.setNavigationLocation(grid.Grid.NAVIGATION_FOOTER);
                    event.preventDefault();
                    gridImpl.focusCell(currentColumnIndex, gridImpl.getFocusedRowIndex());
                    break;
                case grid.Grid.NAVIGATION_FOOTER:
                    break;
                case grid.Grid.NAVIGATION_HEADER:
                    gridImpl.setNavigationLocation(grid.Grid.NAVIGATION_BODY);
                    event.preventDefault();
                    gridImpl.focusCell(currentColumnIndex, gridImpl.getFocusedRowIndex());
                    break;
            }
        }
    }



}