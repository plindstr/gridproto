class TouchContextMenuBehavior extends grid.Behavior{
    
    private contextMenuWidth = 53;
    
    private originX = 0;
    private originY = 0;
    private deltaX = 0;
    
    // 0 undetermined
    // 1 vertical
    // 2 horizontal
    private dragState = 0;
    private dragStateThreshold = 10; // px
    
    private row: HTMLTableRowElement;
    private menu: HTMLElement;
    private menu2: HTMLElement;    
    
    private grid : grid.Grid;
    
    public constructor(grid : grid.Grid) {
        super();
        this.grid = grid;
        
        this.menu = document.createElement("div");
        this.menu.style.position = "absolute";
        this.menu.style.display = "none";
        this.menu.style.width = this.contextMenuWidth+"px";
        util.addStyleName("touch-context-menu", this.menu);
        document.body.appendChild(this.menu);
        
        this.menu2 = <HTMLElement>this.menu.cloneNode(true);
        this.menu2.style.display = "none";
        this.menu2.style.setProperty("-webkit-transform", "scale(-1,1)");
        util.addStyleName("touch-context-menu", this.menu2);
        document.body.appendChild(this.menu2);
        
        this.createMenu(this.menu, 1);
        this.createMenu(this.menu2, this.grid.getColumnCount()-1);
    }
    
    private createMenu(target:HTMLElement, focusIndex:number):void {
        
        var edit : HTMLElement = document.createElement("span");
        edit.innerHTML = "&nbsp;";
        edit.onclick = (e) => {
    
           this.grid.focusCell(1, this.grid.getTable().getRowCoordinate(this.row));
           
           // Invoke row editor by triggering a artificial enter
           var event : KeyboardEvent = <KeyboardEvent> document.createEvent("HTMLEvents");
           event.initEvent("keydown", true, true);
           event.keyCode = KeyCodes.ENTER;           
           this.grid.getFocusedCell().dispatchEvent(event);
           
           // Reset context menu
           this.slideRow(0);
          
        };
        target.appendChild(edit);
    }
    
    public touchStartEvent(g: grid.Grid, event: grid.TouchEvent) {
        if (event.touches.length != 1) return;
        
        var elem = <HTMLElement>event.touches.item(0).target;
     
        var found = false;
        while (elem !== null) {
            switch(elem.tagName) {
                case "THEAD":
                case "TFOOT": {elem = null;}
                case "TBODY": {found = true; break;}
            };
            
            if (found) break;
            elem = elem.parentElement;
        }

        this.dragState = 0;
        this.clipMenu(0, this.menu);
        this.clipMenu(0, this.menu2);
        
        // this is the previous row.
        if (this.row != null) {
            this.row.style.removeProperty("-webkit-transform");
        }

        if (elem == null) {
            this.row = null;
            return;
        }
        
        this.row = this.getRowElement(event.touches.item(0).target);
        this.originX = event.touches.item(0).clientX;
        this.originY = event.touches.item(0).clientY;
        this.deltaX = 0;
        
        if (this.row !== null) {
            var rect = this.row.getBoundingClientRect();
            var top = rect.top;
            var height = rect.bottom-rect.top;
            var left = rect.left;
            var right = rect.right;
            
            this.menu.style.top = (document.body.scrollTop + top)+"px";
            this.menu.style.left = left+"px";
            this.menu.style.height = height+"px";
            this.menu.style.display = "";
            
            this.menu2.style.top = (document.body.scrollTop + top)+"px";
            this.menu2.style.left = (right-this.contextMenuWidth)+"px";
            this.menu2.style.height = height+"px";
            this.menu2.style.display = "";
        }
    }

    public touchMoveEvent(g: grid.Grid, event: grid.TouchEvent) {
        if (this.row === null) return;
        
        if (this.dragState === 0) {
            this.checkDragState(event);
            return;
        } else if (this.dragState === 1) {
            return;
        }
        
        var tr = this.getRowElement(event.touches.item(0).target);
        
        var x = event.touches.item(0).clientX;
        var y = event.touches.item(0).clientY;
        this.deltaX = Math.max(-this.contextMenuWidth, Math.min(this.contextMenuWidth, x-this.originX));
        this.slideRow(this.deltaX);
        
        event.preventDefault();
    }
    
    private checkDragState(event: grid.TouchEvent) {
        if (this.dragState !== 0) return;
        
        var x = event.touches.item(0).clientX;
        var y = event.touches.item(0).clientY;
        var deltaX = Math.abs(x-this.originX);
        var deltaY = Math.abs(y-this.originY);
        
        if (deltaY > deltaX && deltaY > this.dragStateThreshold) this.dragState = 1;
        else if (deltaX > deltaY && deltaX > this.dragStateThreshold) this.dragState = 2;
    }

    public touchEndEvent(g: grid.Grid, event: grid.TouchEvent) {
        if (event.touches.length !== 0) return;
        
        if (this.row !== null) {
            if (this.deltaX < 0 && this.deltaX < -this.contextMenuWidth/2) {
                this.slideRow(-this.contextMenuWidth);
            } else if (this.deltaX > 0 && this.deltaX > this.contextMenuWidth/2) {
                this.slideRow(this.contextMenuWidth);
            } else {
                this.slideRow(0);
            }
        }
    }
    
    private getRowElement(target: EventTarget): HTMLTableRowElement {
        var e = (<HTMLElement>target);
        while (e != null) {
            if (e.tagName == "TR") return <HTMLTableRowElement>e;
            else e = e.parentElement;
        }
        return null;
    }
    
    private slideRow(px: number) {
        if (px != 0) {
            this.row.style.setProperty("-webkit-transform", "translate3d("+px+"px, 0, 0)");
        } else {
            this.row.style.removeProperty("-webkit-transform");
            this.menu.style.display = "none";
            this.menu2.style.display = "none";
        }
        
        this.clipMenu(px >= 0 ? px : 0, this.menu);
        this.clipMenu(px < 0 ? -px : 0, this.menu2);
    }
    
    private clipMenu(px: number, div: HTMLElement) {
        div.style.clip = "rect(0 "+px+"px 100px 0)";
    }
}