class MultiSelectionBehavior extends grid.Behavior {
    
    private uncheckedBox = "imgs/check_deselected.png";
    private checkedBox = "imgs/check_selected.png";
    private undefinedBox = "imgs/check_indeterminate.png";
    
    private lastSingleToggledRow: number;
    private lastSingleToggledRowForMoveEvent = -1;
    private lastRangeEnd: number;
    
    private dragShouldSelect = false;
    private mouseIsDown = false;
    private touchIsDown = false;
    private spaceIsDown = false;
    
    private gridHasAllSelected(grid: grid.Grid) {
        for (var row = 0; row < grid.getRowCount(); row++) {
            if (!grid.isCellSelected(0, row)) return false;
        }
        return true;
    }
    
    private gridHasAnySelected(grid: grid.Grid) {
        for (var row = 0; row < grid.getRowCount(); row++) {
            if (grid.isCellSelected(0, row)) return true;
        }
        return false;
    }
    
    private toggleSingleSelect(row: number, grid: grid.Grid) {
        if (grid.isCellSelected(0, row)) {
            this.unselectRow(row, grid);
        } else {
            this.selectRow(row, grid);
        }
    }
    
    private unselectRow(row: number, grid: grid.Grid) {
        (<HTMLElement>grid.getCell(0, row).children.item(0)).style.backgroundImage="url(\""+this.uncheckedBox+"\")";
        for (var i=0; i < grid.getColumnCount(); i++) {
            grid.unselectCell(i, row);
        }
    }
    
    private selectRow(row: number, grid: grid.Grid) {
        (<HTMLElement>grid.getCell(0, row).children.item(0)).style.backgroundImage="url(\""+this.checkedBox+"\")";
        for (var i=0; i < grid.getColumnCount(); i++) {
            grid.selectCell(i, row);
        }
    }
    
    private unsetRangeSelect(fromRow: number, toRow: number, grid: grid.Grid) {
        for (var row = fromRow; row <= toRow; row++) {
            this.unselectRow(row, grid);
        }
    }
    
    private setRangeSelect(fromRow: number, toRow: number, grid: grid.Grid) {
        for (var row = fromRow; row <= toRow; row++) {
            this.selectRow(row, grid);
        }
    }
    
    public headerClicked(grid: grid.Grid, cell: HTMLElement, column: number, row: number, event: MouseEvent) {
        if (column != 0) return;
        this.clickSelectAllCell(grid);
        event.preventDefault();
    }
    
    private clickSelectAllCell(grid: grid.Grid) {
        var cell = grid.getHeaderCell(0,0);
        if ((<HTMLElement>cell.firstElementChild).style.backgroundImage.lastIndexOf(this.uncheckedBox) != -1) {
            for (var row = 0; row < grid.getRowCount(); row++) {
                this.selectRow(row, grid);
            }
        } else {
            for (var row = 0; row < grid.getRowCount(); row++) {
                this.unselectRow(row, grid);
            }
        }
        
        this.checkHeaderIconState(grid);
    }
    
    /* this should probably be called in both selectRow and 
    deselectRow instead of everywhere calling those methods. 
    Oh well... */
    private checkHeaderIconState(grid: grid.Grid) {
        if (this.gridHasAllSelected(grid)) {
            (<HTMLElement>grid.getHeaderCell(0, 0).children.item(0)).style.backgroundImage="url(\""+this.checkedBox+"\")";
        } else if (this.gridHasAnySelected(grid)) {
            (<HTMLElement>grid.getHeaderCell(0, 0).children.item(0)).style.backgroundImage="url(\""+this.undefinedBox+"\")";
        } else {
            (<HTMLElement>grid.getHeaderCell(0, 0).children.item(0)).style.backgroundImage="url(\""+this.uncheckedBox+"\")";
        }
    }
    
    public mouseDownEvent(grid: grid.Grid, event: MouseEvent) {
        if (event.button != 0) {
            // Ignore unless primary button 
            return;
        }
        
        var coords = grid.getBodyCoordinatesFor(event.toElement);
        if (coords == null) return;
        
        var row = coords[0];
        var column = coords[1];
        
        // only on checkbox. Or, if we've already selected one row, we can shift-click on any cell.
        if (!(column == 0 || (grid.isCellSelected(0,this.lastRangeEnd|0) && event.shiftKey))) return;

        this.mouseIsDown = true;
        this.dragShouldSelect = !grid.isCellSelected(column, row);
        this.lastSingleToggledRowForMoveEvent = row;
        
        if (!event.shiftKey) {
            this.lastSingleToggledRow = row;
            this.lastRangeEnd = row;
            this.toggleSingleSelect(row, grid);
        } else {
            var from = Math.min(this.lastSingleToggledRow, this.lastRangeEnd);
            var to = Math.max(this.lastSingleToggledRow, this.lastRangeEnd);
            this.unsetRangeSelect(from, to, grid); 

            this.lastRangeEnd = row;
            var newFrom = Math.min(this.lastSingleToggledRow, this.lastRangeEnd);
            var newTo = Math.max(this.lastSingleToggledRow, this.lastRangeEnd);
            this.setRangeSelect(newFrom, newTo, grid);
        }
        
        this.checkHeaderIconState(grid);
    }
    
    public mouseUpEvent(grid: grid.Grid, event: MouseEvent) {
        this.mouseIsDown = false;
        this.lastSingleToggledRowForMoveEvent = -1;
    }
    
    public mouseMoveEvent(grid: grid.Grid, event: MouseEvent) {
        if (!this.mouseIsDown) return;
        
        var coords = grid.getBodyCoordinatesFor(event.toElement);
        if (coords == null) return;
        var row = coords[0];
        var col = coords[1];
        
        if (this.lastSingleToggledRowForMoveEvent != row) {
            var from = Math.min(this.lastSingleToggledRowForMoveEvent, row);
            var to = Math.max(this.lastSingleToggledRowForMoveEvent, row);
            this.lastSingleToggledRowForMoveEvent = row;
            
            if ((grid.isCellSelected(0, row) && !this.dragShouldSelect)
                    || !grid.isCellSelected(0, row) && this.dragShouldSelect) {
                this.lastSingleToggledRow = row;
                this.lastRangeEnd = row;
            }
            
            if (this.dragShouldSelect) {
                for (var i=from; i<=to; i++) this.selectRow(i, grid);
            } else {
                for (var i=from; i<=to; i++) this.unselectRow(i, grid);
            }
            
            this.checkHeaderIconState(grid);
            grid.focusCell(col, row);
        }
    }
    
    public keyboardEvent(g: grid.Grid, event: KeyboardEvent) {
        if (event.type == "keydown") this.keydown(g, event);
        else if (event.type == "keyup") this.keyup(g, event);
    }
    
    private keydown(g: grid.Grid, event: KeyboardEvent) {
        if (g.getNavigationLocation() == grid.Grid.NAVIGATION_FOOTER) return;

        else if (g.getNavigationLocation() == grid.Grid.NAVIGATION_HEADER) {
            if(g.getFocusedColumnIndex() == 0){
                switch(event.keyCode){
                    case KeyCodes.SPACE:
                    case KeyCodes.F8:
                        this.clickSelectAllCell(g);
                        event.preventDefault();
                }
            }
        }
        
        else {
            var coords = g.getBodyCoordinatesFor(g.getFocusedCell());
            if (coords == null) return;
            var row = coords[0];
            var col = coords[1];
            
            switch(event.keyCode){
                case KeyCodes.SPACE:
                case KeyCodes.F8:
                    this.spaceIsDown = true;
                    this.toggleSingleSelect(row, g);
                    this.checkHeaderIconState(g);
                    this.lastSingleToggledRow = row;
                    this.dragShouldSelect = g.isCellSelected(0, row);
                    event.preventDefault();
                    break;
                    
                case KeyCodes.SHIFT:
                    this.lastSingleToggledRow = row;
                    this.lastRangeEnd = row;
                    break;
                
                case KeyCodes.HOME:
                case KeyCodes.END:
                case KeyCodes.PG_UP:
                case KeyCodes.PG_DOWN:
                case KeyCodes.ARROW_UP:
                case KeyCodes.ARROW_DOWN:
                    if (event.shiftKey) {
                        var from = Math.min(this.lastSingleToggledRow, this.lastRangeEnd);
                        var to = Math.max(this.lastSingleToggledRow, this.lastRangeEnd);
                        this.unsetRangeSelect(from, to, g); 
                
                        this.lastRangeEnd = row;
                        var newFrom = Math.min(this.lastSingleToggledRow, this.lastRangeEnd);
                        var newTo = Math.max(this.lastSingleToggledRow, this.lastRangeEnd);
                        this.setRangeSelect(newFrom, newTo, g);
                        
                        event.preventDefault();
                        this.checkHeaderIconState(g);
                    } else if (this.spaceIsDown) {
                        if (this.dragShouldSelect) {
                            this.selectRow(row, g);
                        } else {
                            this.unselectRow(row, g);
                        }
                        this.checkHeaderIconState(g);
                    }
                    break;                    
            }    
        }
    }
    
    private keyup(g: grid.Grid, event: KeyboardEvent) {
        if (event.keyCode == KeyCodes.SPACE) {
            this.spaceIsDown = false;
        }
    }
    
    public touchStartEvent(g: grid.Grid, event: grid.TouchEvent) {
        if (event.touches.length != 1) return;
        
        // nagivationlocation doesn't work for headers/footers currently. Searching manually.
        var location = g.getLocationOf(<Element>event.touches.item(0).target);
        if (location === grid.Grid.NAVIGATION_FOOTER) {
            return;
        }
        
        else if (location === grid.Grid.NAVIGATION_HEADER) {
            var cellElem = <Element>event.touches.item(0).target;
            while (cellElem.tagName !== "TH") {
                cellElem = <Element>cellElem.parentNode;
            }
            
            if (cellElem.previousSibling === null) {
                this.clickSelectAllCell(g);
                event.preventDefault();
            }
        }
        
        else {
            var coords = g.getBodyCoordinatesFor(<Element>event.targetTouches.item(0).target);
            if (coords == null) return;
            var row = coords[0];
            var col = coords[1];
            if (col != 0) return;        
            
            this.touchIsDown = true;
            this.dragShouldSelect = !g.isCellSelected(col, row);
            this.toggleSingleSelect(row, g);
            this.checkHeaderIconState(g);
            this.lastSingleToggledRow = row;
            this.lastSingleToggledRowForMoveEvent = row;
            event.preventDefault();
        }
    }

    public touchMoveEvent(grid: grid.Grid, event: grid.TouchEvent) {
        if (!this.touchIsDown) {
            return;
        }
        event.preventDefault();        

        var element = document.elementFromPoint(event.touches.item(0).clientX, event.touches.item(0).clientY);
        var coords = grid.getBodyCoordinatesFor(element);
        if (coords == null) return;
        var row = coords[0];
        var col = coords[1];
        
        if (this.lastSingleToggledRowForMoveEvent != row) {
            var from = Math.min(this.lastSingleToggledRowForMoveEvent, row);
            var to = Math.max(this.lastSingleToggledRowForMoveEvent, row);
            this.lastSingleToggledRowForMoveEvent = row;
            
            if ((grid.isCellSelected(col, row) && !this.dragShouldSelect)
                    || !grid.isCellSelected(col, row) && this.dragShouldSelect) {
                this.lastSingleToggledRow = row;
                this.lastRangeEnd = row;
            }
            
            if (this.dragShouldSelect) {
                for (var i=from; i<=to ; i++) this.selectRow(i, grid);
            } else {
                for (var i=from; i<= to; i++) this.unselectRow(i, grid);
            }
            
            this.checkHeaderIconState(grid);
            grid.focusCell(col, row);        
        }
    }

    public touchEndEvent(grid: grid.Grid, event: grid.TouchEvent) {
        if (event.targetTouches.length != 0) return;
        
        this.touchIsDown = false;
        
        // absolutely never put a preventDefault here! it breaks touch scrolling!
        // event.preventDefault();
    }    
}
