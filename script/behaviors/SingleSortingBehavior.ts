/**
 * Single-item sorting behavior
 */

class SingleSortingBehavior extends grid.Behavior {


    private _sortBy: number;

    /**
     * Called when a cell in a header is clicked
     */
    public headerClicked(grid: grid.Grid, cell: HTMLElement, column: number, row: number, event: MouseEvent) {
        // Ignore selection box column...
        if (!grid.isColumnSortable(column)) {
            return;
        }

        this.doSort(grid, cell, column);
    }

    public headerKeyPressed(grid: grid.Grid, cell: HTMLElement, keyCode: number, column: number, row: number, event: KeyboardEvent) {
        // Ignore selection box column...
        if (!grid.isColumnSortable(column)) {
            return;
        }

        if (keyCode == KeyCodes.SPACE) {
            this.doSort(grid, cell, column);
            event.preventDefault();
        }
    }

    private doSort(grid: grid.Grid, cell:HTMLElement, column: number) {
        if (column == this._sortBy) {
            grid.sortByColumn(column, (b, a) => {
                return a.textContent.localeCompare(b.textContent, [], { numeric: true });
            });
            this._sortBy = null;
        } else {
            grid.sortByColumn(column, (a, b) => {
                return a.textContent.localeCompare(b.textContent, [], { numeric: true });
            });
            this._sortBy = column;
        }
    }

}
