class SingleSelectionBehavior extends grid.Behavior {

    private uncheckedBox = "\u2610"; // "☐" // "&#x2610;";
    private checkedBox = "\u2611"; // "☑" // "&#x2611;";
    private undefinedBox = "\u25a0"; // "■" // "&#x25a0;";

    private prevSelected = -1;

    public gridHasAllSelected(grid: grid.Grid) {
        for (var row = 0; row < grid.getRowCount(); row++) {
            if (!grid.isCellSelected(0, row)) return false;
        }
        return true;
    }

    public gridHasAnySelected(grid: grid.Grid) {
        for (var row = 0; row < grid.getRowCount(); row++) {
            if (grid.isCellSelected(0, row)) return true;
        }
        return false;
    }

    public toggleSingleSelect(row: number, grid: grid.Grid) {
        if (grid.isCellSelected(0, row)) {
            this.unselectRow(row, grid);
        } else {
            this.selectRow(row, grid);
        }
    }

    public unselectRow(row: number, grid: grid.Grid) {
        grid.getCell(0, row).innerHTML = this.uncheckedBox;
        for (var i = 0; i < grid.getColumnCount(); i++) {
            grid.unselectCell(i, row);
        }
    }

    public selectRow(row: number, grid: grid.Grid) {
        grid.getCell(0, row).innerHTML = this.checkedBox;
        for (var i = 0; i < grid.getColumnCount(); i++) {
            grid.selectCell(i, row);
        }
    }

    public checkHeaderIconState(grid: grid.Grid) {
        if (this.gridHasAllSelected(grid)) {
            grid.getHeaderCell(0,0).innerHTML = this.checkedBox;
        } else if (this.gridHasAnySelected(grid)) {
            grid.getHeaderCell(0,0).innerHTML = this.undefinedBox;
        } else {
            grid.getHeaderCell(0,0).innerHTML = this.uncheckedBox;
        }
    }

    public mouseDownEvent(grid: grid.Grid, event: MouseEvent) {
        var coords = grid.getBodyCoordinatesFor(event.toElement);
        if (coords == null) return;

        var row = coords[0];
        var column = coords[1];

        if (column != 0) return;

        if (this.prevSelected != -1) this.unselectRow(this.prevSelected, grid);
        this.selectRow(row, grid);
        this.checkHeaderIconState(grid);
        this.prevSelected = row;
    }
}