class ActionableModeBehavior extends grid.Behavior{
    
    public activeInActionableMode() : boolean {
            return true;    
    }
    
    public mouseDownEvent(gridImpl: grid.Grid, event: MouseEvent) {
         
         if(gridImpl.getNavigationMode() == grid.Grid.NAVIGATION_MODE_ACTIONABLE){
            // Clicking outside of actionable mode should terminate actionable mode             
             var focused = gridImpl.getFocusedCell();
             if(!ActionableModeBehavior.isInlineWidgetCell(focused) && !focused.contains(<HTMLElement>event.currentTarget)){
                 ActionableModeBehavior.replaceFieldWithContent(focused);
             }
             
            //Remove stylename
            util.removeStyleName("actionable", focused);
            
            //Refocus table
            gridImpl.getTable().focus();
            
             // Switch to navigation mode back
            gridImpl.setNavigationMode(grid.Grid.NAVIGATION_MODE_NAVIGATION);

            // Focus clicked cell
            var coords : number[] = gridImpl.getLocationDependentCoordinatesFor(event.toElement);
            if (coords == null) return;
            var row = coords[0];
            var column = coords[1];

            gridImpl.focusCell(column, row);
            
            event.preventDefault();
 
         }   
     }
    
    /**
     * Called when a cell in the main table body is clicked
     */
     public cellClicked(gridImpl: grid.Grid, cell: HTMLElement, column: number, row: number, event: MouseEvent) {
        if(column == 0 || gridImpl.getNavigationLocation() != grid.Grid.NAVIGATION_BODY){
            // Don't trigger actionable mode on checkbox column
            return;    
        }
         
         if(gridImpl.getNavigationMode() == grid.Grid.NAVIGATION_MODE_NAVIGATION){
            if(event.detail == 2){
                // Double click enters actionable mode
                gridImpl.setNavigationMode(grid.Grid.NAVIGATION_MODE_ACTIONABLE);
                
                // Switch to input field
                if(cell != null && !ActionableModeBehavior.isInlineWidgetCell(cell)){
                     ActionableModeBehavior.replaceContentWithField(cell);
                }
                
                // Add style name
                util.addStyleName("actionable", cell);
                
                // Focus content
                if(!ActionableModeBehavior.isInlineWidgetCell(cell)){
                    var input : HTMLInputElement = <HTMLInputElement> cell.firstElementChild;
                    input.focus();          
                    input.setSelectionRange(0,input.value.length); 
                } else {
                    var input : HTMLInputElement = <HTMLInputElement> cell.firstElementChild.firstElementChild;                        
                    input.focus();
                }
                
                event.preventDefault();
            }    
             
         }
         
     }

     public cellKeyPressed(gridImpl: grid.Grid, cell: HTMLElement, keyCode: number, column: number, row: number, event: KeyboardEvent) {
        if(column == 0){
            // Don't trigger actionable mode on checkbox column
            return;    
        }
         
        if(gridImpl.getNavigationMode() == grid.Grid.NAVIGATION_MODE_ACTIONABLE){     
            switch(keyCode){              
                case KeyCodes.TAB:
                    // Prevent tabbing out from actionable mode
                    var active : HTMLElement = <HTMLElement>document.activeElement;
                    if(event.shiftKey){
                        if(active == active.parentNode.firstChild){     
                            event.preventDefault();
                            (<HTMLElement>active.parentNode.lastChild).focus();
                        }
                    } else {
                        if(active == active.parentNode.lastChild){     
                            event.preventDefault();
                            (<HTMLElement>active.parentNode.firstChild).focus();
                        }
                    }
                    break;
                        
                case KeyCodes.ENTER:
                case KeyCodes.ESCAPE:
                    // Readonly content
                    if(!ActionableModeBehavior.isInlineWidgetCell(cell)){
                        ActionableModeBehavior.replaceFieldWithContent(cell);
                    }
                    
                    //Remove stylename
                    util.removeStyleName("actionable", cell);
                    
                    //Refocus table
                    gridImpl.getTable().focus();
                    
                     // Switch to navigation mode back
                    gridImpl.setNavigationMode(grid.Grid.NAVIGATION_MODE_NAVIGATION);
                    
                    event.preventDefault();
                    break;                
            }
            
        } else if(gridImpl.getNavigationMode() == grid.Grid.NAVIGATION_MODE_NAVIGATION){
            switch(keyCode){
                case KeyCodes.ENTER:
                case KeyCodes.F2:
                    // Switch to actionable mode
                    gridImpl.setNavigationMode(grid.Grid.NAVIGATION_MODE_ACTIONABLE);
                    
                    // Switch to input field
                    if(!ActionableModeBehavior.isInlineWidgetCell(cell)){
                         ActionableModeBehavior.replaceContentWithField(cell);
                    }
                    
                    // Add style name
                    util.addStyleName("actionable", cell);
                    
                    // Focus content
                    if(!ActionableModeBehavior.isInlineWidgetCell(cell)){
                        var input : HTMLInputElement = <HTMLInputElement> cell.firstElementChild;
                        input.focus();          
                        input.setSelectionRange(0,input.value.length); 
                    } else {
                        var input : HTMLInputElement = <HTMLInputElement> cell.firstElementChild.firstElementChild;                        
                        input.focus();
                    }
                    
                    event.preventDefault();
                    break;
            }
        }
     }
    
     public static isInlineWidgetCell(cell : HTMLElement) : boolean {
         return (<HTMLElement>cell.firstElementChild).className == "inline-widget";    
     }
    
     public static replaceContentWithField (cell : HTMLElement) :void {
        var content :string = cell.innerText;        
      
        var input : HTMLInputElement = document.createElement('input');
        input.value = content;
         
        cell.innerHTML = "";
        cell.appendChild(input);
     }
    
     public static replaceFieldWithContent(cell:HTMLElement) : void {
        var input : HTMLInputElement = <HTMLInputElement> cell.firstElementChild;
        var content : string = input.value;
        cell.innerHTML = "<span>"+content+"<span>";   
     }
    
     public static replaceContentWithSelect(cell : HTMLElement, data: string[]) : void {
        var content :string = cell.innerText;        
       
        var select : HTMLSelectElement = document.createElement('select');
        
        for (var i: number = 0; i < data.length; i++) {
            var option : HTMLOptionElement = document.createElement('option');
            option.value = i.toString();
            option.innerText = data[i];
            
            if(data[i] == content){
                option.selected = true;    
            }
            select.appendChild(option);
        }
         
        cell.innerHTML = "";
        cell.appendChild(select); 
     }
    
     public static replaceSelectWithContent(cell:HTMLElement, data: string[]) : void {
        var select : HTMLSelectElement = <HTMLSelectElement> cell.firstElementChild;
        var content : string = data[parseInt(select.value)];
        cell.innerHTML = "<span>"+content+"<span>";    
         
     }
    
}