/**
 * Multi-row sorting behavior
 */

class MultiSortingBehavior extends grid.Behavior {

    private _sortBy: {
        column: number;
        order: boolean;
    }[] = [];

    public headerClicked(grid: grid.Grid, cell: HTMLElement, column: number, row: number, event: MouseEvent) {
        if (!grid.isColumnSortable(column)) {
            console.log("Column  " + column + " is not sortable; ignored");
            return;
        }

        this.toggleSortOrder(grid, column);
        this.doSort(grid);
    }

    public headerKeyPressed(grid: grid.Grid, cell: HTMLElement, keyCode: number, column: number, row: number, event: KeyboardEvent) {
        if (!grid.isColumnSortable(column)) {
            console.log("Column  " + column + " is not sortable; ignored");
            return;
        }

        if (keyCode == KeyCodes.SPACE) {
            this.toggleSortOrder(grid, column);
            this.doSort(grid);
            event.preventDefault();
        }
    }

    private isColumnTracked(column: number) {
        for (var i = 0, l = this._sortBy.length; i < l; ++i) {
            if (this._sortBy[i].column === column) {
                return true;
            }
        }
        return false;
    }

    private getColumnOrder(column: number) {
        for (var i = 0, l = this._sortBy.length; i < l; ++i) {
            if (this._sortBy[i].column === column) {
                return this._sortBy[i].order ? 1 : 0;
            }
        }
        return -1;
    }
    
    private setColumnOrder(column: number, order: boolean) {
        for (var i = 0, l = this._sortBy.length; i < l; ++i) {
            if (this._sortBy[i].column === column) {
                this._sortBy[i].order = order;
            }
        }
        return -1;
    }

    private removeColumn(column: number) {
        for (var i = 0, l = this._sortBy.length; i < l; ++i) {
            if (this._sortBy[i].column === column) {
                this._sortBy.splice(i, 1);
                return;
            }
        }
    }

    private addColumn(column: number, order: boolean) {
        if (this.isColumnTracked(column)) {
            this.setColumnOrder(column,order);
            return;
        }

        this._sortBy.push({
            column: column,
            order: order
        });
    }

    private MAGIC_ROW = 0;
    
    private toggleSortOrder(grid: grid.Grid, column: number) {
        var cell = grid.getHeaderCell(column, this.MAGIC_ROW);
        this.clearStyleNames(cell);
        if (this.isColumnTracked(column)) {
            if (this.getColumnOrder(column)) {
                this.addColumn(column, false);
                util.addStyleName('v-grid-header-sorted-desc', cell);
            } else {
                this.removeColumn(column);
            }
        } else {
            this.addColumn(column, true);
            util.addStyleName('v-grid-header-sorted-asc', cell);
        }

        this.updateOrderMarkers(grid);
    }

    private updateOrderMarkers(grid: grid.Grid) {

        for (var i = 0; i < grid.getColumnCount(); ++i) {
            if (grid.isColumnSortable(i)) {
                var extra = grid.getHeaderCellExtra(i, this.MAGIC_ROW);
                extra.innerHTML = "";
            }
        }

        for (var i = 0; i < this._sortBy.length; ++i) {
            var extra = grid.getHeaderCellExtra(this._sortBy[i].column, this.MAGIC_ROW);
            extra.innerHTML = "" + (i + 1);
        }

    }

    private clearStyleNames(cell: HTMLElement) {
        util.removeStyleName('v-grid-header-sorted-asc', cell);
        util.removeStyleName('v-grid-header-sorted-desc', cell);
    }

    private handleSpecial(elem:HTMLElement):string {
    
        var inputElem = <HTMLInputElement>util.findElementUp("INPUT",elem);
        
        if(inputElem.checked !== undefined) {
            return (inputElem.checked == true) ? "1" : "2";
        }
        
        var selectElem = <HTMLSelectElement> util.findElementUp("SELECT",elem);
        
        console.log(selectElem);
        
        if(selectElem.selectedIndex !== undefined) {
            var value = "" + selectElem.selectedIndex;
            console.log("Found select element with value " + value);
            return value;
        }
        
        return "999999";
    }
    
    private doSort(grid: grid.Grid) {

        if (this._sortBy.length === 0) return;

        grid.sortByRow((row_a, row_b) => {

            var a: string, b: string, result: number;
            for (var i = 0; i < this._sortBy.length; ++i) {
                var col = this._sortBy[i].column;
                a = row_a[col].textContent;
                b = row_b[col].textContent;
                
                if(a === '' && b === '') {
                    a = this.handleSpecial(row_a[col]);
                    b = this.handleSpecial(row_b[col]);
                }

                if (this._sortBy[i].order) {
                    result = a.localeCompare(b, [], { numeric: true });
                } else {
                    result = b.localeCompare(a, [], { numeric: true });
                }

                if (result !== 0) return result;
            }

            return 0;
        });

    }

}
