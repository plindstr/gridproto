class BodyTouchScrollBehavior extends grid.Behavior {
    
    private startY = -1;
    private startScrollTop = -1;
    private tbody: HTMLTableSectionElement = null;
    
    public touchStartEvent(g: grid.Grid, event: grid.TouchEvent) {
        if (event.touches.length !== 1) return;
        
        var e = <HTMLElement>event.touches.item(0).target;
        do {
            if (e === null || e.tagName === "TABLE") return;
            e = e.parentElement;
        } while (e.tagName !== "TBODY");
        
        this.tbody = <HTMLTableSectionElement> e;
        this.startY = event.touches.item(0).clientY;
        this.startScrollTop = e.scrollTop; 
    }
    
    public touchMoveEvent(g: grid.Grid, event: grid.TouchEvent) {
        if (this.startY === -1 || event.defaultPrevented) return;
        
        var deltaY = event.touches.item(0).clientY-this.startY;
        this.tbody.scrollTop = this.startScrollTop-deltaY;
    }
    
    public touchEndEvent(g: grid.Grid, event: grid.TouchEvent) {
        if (event.touches.length !== 0) return;
        
        this.startY = -1;
        this.startScrollTop = -1;
        this.tbody = null;
    }
}