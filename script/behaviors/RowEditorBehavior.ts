class RowEditorBehavior extends grid.Behavior {

    private oldValues: Array<string>;

    private rowEditorControls: HTMLElement;

    private currentRow: HTMLElement;

    private topShim: HTMLElement;

    private bottomShim: HTMLElement;

    private editorIsVisible: boolean = false;

    private grid: grid.Grid;

    public constructor(g: grid.Grid) {
        super();

        this.grid = g;

        this.topShim = document.createElement("div");
        this.topShim.style.display = "none";
        util.addStyleName("row-editor-shim", this.topShim);

        this.topShim.onmousedown = (e) => {
            // Prevent focus from leaving editor row when clicking on shim
            e.preventDefault();
        }

        document.body.appendChild(this.topShim);

        this.bottomShim = document.createElement("div");
        this.bottomShim.style.display = "none";
        util.addStyleName("row-editor-shim", this.bottomShim);

        this.bottomShim.onmousedown = (e) => {
            // Prevent focus from leaving editor row when clicking on shim
            e.preventDefault();
        }

        document.body.appendChild(this.bottomShim);

        this.rowEditorControls = document.createElement("div");
        this.rowEditorControls.style.display = "none";
        util.addStyleName("v-grid-row-editor", this.rowEditorControls);

        this.rowEditorControls.onmousedown = (e) => {
            // Prevent focus from leaving editor row when clicking on controls
            e.preventDefault();
        }

        document.body.appendChild(this.rowEditorControls);

        var save: HTMLElement = document.createElement("span");
        save.innerText = "Save";
        save.tabIndex = 0;

        save.onclick = (e) => {
            this.save();
        };

        save.onkeydown = (e) => {
            if (e.keyCode == KeyCodes.TAB && e.shiftKey) {
                // focus last cell        
                var grid: grid.Grid = this.grid;
                var self: RowEditorBehavior = this;
                setTimeout(function() {
                    grid.focusCell(grid.getColumnCount() - 1, grid.getFocusedRowIndex());
                    self.focusCellContent(grid.getFocusedCell(), true);
                }, 0, grid, self);
                e.preventDefault();
            } else if (e.keyCode == KeyCodes.ENTER) {
                this.save();
                e.preventDefault();
            }
        };

        this.rowEditorControls.appendChild(save);

        var cancel: HTMLElement = document.createElement("span");
        cancel.innerText = "Cancel";
        cancel.tabIndex = 0;

        cancel.onclick = (e) => {
            this.cancel();
        };

        cancel.onkeydown = (e) => {
            if (e.keyCode == KeyCodes.TAB && !e.shiftKey) {
                // focus first cell
                var grid: grid.Grid = this.grid;
                var self: RowEditorBehavior = this;
                setTimeout(function() {
                    grid.focusCell(1, grid.getFocusedRowIndex());
                    self.focusCellContent(grid.getFocusedCell());
                }, 0, grid, self);
                e.preventDefault();

            } else if (e.keyCode == KeyCodes.ENTER) {
                this.cancel();
                e.preventDefault();
            }
        }

        this.rowEditorControls.appendChild(cancel);
    }

    private showEditor(cell: HTMLElement): void {

        this.rowEditorControls.style.display = "inherit";
        this.topShim.style.display = "inherit";
        this.bottomShim.style.display = "inherit";

        var tbody: HTMLElement = cell.parentElement.parentElement;
        tbody.style.overflow = "hidden";

        this.currentRow = cell.parentElement;
        this.currentRow.style.width = (tbody.offsetWidth - 15) + "px";

        // prevent wheel scrolling while editing
        this.currentRow.onmousewheel = (e) => {
            e.preventDefault();
            e.stopPropagation();
            return false;
        }

         var headerHeight: number = tbody.offsetTop;
        var rowTop: number = this.currentRow.offsetTop;
        var rowHeight: number = this.currentRow.offsetHeight;
        var top: number = headerHeight + rowTop + rowHeight - tbody.scrollTop + 9;
        var left: number = (<HTMLElement>this.currentRow.lastChild).offsetLeft
            + (<HTMLElement>this.currentRow.lastChild).offsetWidth
            - this.rowEditorControls.offsetWidth + 7;

        this.rowEditorControls.style.top = top + "px";
        this.rowEditorControls.style.left = left + "px";

        this.topShim.style.width = (tbody.offsetWidth) + "px";
        this.topShim.style.height = (top - rowHeight - headerHeight - 2) + "px";
        this.topShim.style.top = headerHeight + "px";

        this.bottomShim.style.top = (top - 2) + "px";
        this.bottomShim.style.width = this.topShim.style.width;
        this.bottomShim.style.height = (tbody.offsetHeight - top + headerHeight + rowHeight) + "px";

        this.editorIsVisible = true;

        util.addStyleName("actionable-row", this.currentRow);

        console.log("Editor row opened");
    }

    private hideEditor(cell: HTMLElement): void {
        this.rowEditorControls.style.display = "none";
        this.topShim.style.display = "none";
        this.bottomShim.style.display = "none";

        var tbody: HTMLElement = cell.parentElement.parentElement;

        this.editorIsVisible = false;

        util.removeStyleName("actionable-row", this.currentRow);

        this.currentRow.onmousewheel = null;
        tbody.style.overflow = "auto";

        console.log("Editor row closed");
    }

    public activeInActionableMode(): boolean {
        return true;
    }

    private cancel(): void {
        // Replace cells with old values 
        for (var columnIndex: number = 1; columnIndex < this.grid.getColumnCount(); columnIndex++) {
            var c: HTMLElement = this.grid.getCell(columnIndex, this.grid.getFocusedRowIndex());

            // Restore content
            if (!ActionableModeBehavior.isInlineWidgetCell(c)) {
                var c: HTMLElement = this.grid.getCell(columnIndex, this.grid.getFocusedRowIndex());
                c.innerHTML = this.oldValues[columnIndex - 1];
            }

            //Remove stylename
            util.removeStyleName("actionable", c);
        }

        // Hide editor
        this.hideEditor(this.grid.getFocusedCell());

        //Refocus table
        this.grid.getTable().focus();

        // Switch to navigation mode mode
        this.grid.setNavigationMode(grid.Grid.NAVIGATION_MODE_NAVIGATION);
    }

    private save(): void {

        // Replace cells with new values 
        for (var columnIndex: number = 1; columnIndex < this.grid.getColumnCount(); columnIndex++) {
            var c: HTMLElement = this.grid.getCell(columnIndex, this.grid.getFocusedRowIndex());

            // Restore content
            if (!ActionableModeBehavior.isInlineWidgetCell(c)) {
                if (columnIndex == 4) {
                    ActionableModeBehavior.replaceSelectWithContent(c, PersonnelGenderString);
                } else {
                    ActionableModeBehavior.replaceFieldWithContent(c);
                }
            }

            //Remove stylename
            util.removeStyleName("actionable", c);
        }

        // Hide editor
        this.hideEditor(this.grid.getFocusedCell());

        //Refocus table
        this.grid.getTable().focus();

        // Switch to navigation mode mode
        this.grid.setNavigationMode(grid.Grid.NAVIGATION_MODE_NAVIGATION);
    }

    public cellKeyDown(gridImpl: grid.Grid, cell: HTMLElement, keyCode: number, column: number, row: number, event: KeyboardEvent) {

        if (column == 0 || event.defaultPrevented) {
            // Don't trigger actionable mode on checkbox column or if something already has handled the event
            return;
        }

        if (gridImpl.getNavigationMode() == grid.Grid.NAVIGATION_MODE_ACTIONABLE) {
            switch (keyCode) {
                case KeyCodes.TAB:

                    if (event.shiftKey && column == 1) {

                        // Jump to cancel
                        (<HTMLElement> this.rowEditorControls.lastElementChild).focus();
                        event.preventDefault();

                    } else {
                        /*
                         * This hack will check where the focus end up and if it ends up somewhere 
                         * outside of the row, then refocus the row start
                         */
                        var editor: HTMLElement = this.rowEditorControls;
                        setTimeout(function() {
                            var active: HTMLElement = <HTMLElement>document.activeElement;
                            var coords: number[] = gridImpl.getLocationDependentCoordinatesFor(active);
                            if (coords == null) return;

                            var r: number = coords[0];
                            var c: number = coords[1];

                            if (r != row) {
                                // Jump to save
                                (<HTMLElement> editor.firstElementChild).focus();
                                event.preventDefault();
                            } else {
                                gridImpl.focusCell(c, r);
                            }
                        }, 0, editor);
                    }
                    break;

                case KeyCodes.F2:
                case KeyCodes.ENTER:
                    this.save();
                    event.preventDefault();
                    break;

                case KeyCodes.ESCAPE:
                    this.cancel();
                    event.preventDefault();
                    break;
            }

        } else if (gridImpl.getNavigationMode() == grid.Grid.NAVIGATION_MODE_NAVIGATION) {
            switch (keyCode) {
                case KeyCodes.F2:
                case KeyCodes.ENTER:

                    this.oldValues = new Array<string>();

                    // Switch to actionable mode
                    gridImpl.setNavigationMode(grid.Grid.NAVIGATION_MODE_ACTIONABLE);

                    // Replace cells with input fields
                    for (var columnIndex: number = 1; columnIndex < gridImpl.getColumnCount(); columnIndex++) {
                        var c: HTMLElement = gridImpl.getCell(columnIndex, row);

                        // Store old value
                        this.oldValues.push(c.innerHTML);

                        // Replace content with input field
                        if (columnIndex == 4) {
                            ActionableModeBehavior.replaceContentWithSelect(c, PersonnelGenderString);

                        } else if (!ActionableModeBehavior.isInlineWidgetCell(c)) {
                            ActionableModeBehavior.replaceContentWithField(c);
                        }

                        // Add style name
                        util.addStyleName("actionable", c);
                    }

                    // Focus content in cell selected
                    this.focusCellContent(cell);

                    // Show editor
                    this.showEditor(cell);

                    event.preventDefault();
                    break;
            }
        }
    }

    private focusCellContent(cell: HTMLElement, last: boolean= false): void {
        if (!ActionableModeBehavior.isInlineWidgetCell(cell)) {
            var input: HTMLInputElement = <HTMLInputElement> cell.firstElementChild;
            input.focus();

            try {
                input.setSelectionRange(0, input.value.length);
            } catch (e) {
                //HACKHACKHACK
            }

        } else {
            var input: HTMLInputElement;
            if (last) {
                input = <HTMLInputElement> cell.firstElementChild.lastElementChild;
            } else {
                input = <HTMLInputElement> cell.firstElementChild.firstElementChild;
            }
            input.focus();
        }
    }

    public mouseDownEvent(gridImpl: grid.Grid, event: MouseEvent) {

        if (event.defaultPrevented) {
            return;
        }

        // Ensure focus stays with fields
        setTimeout(function() {
            var active: HTMLElement = <HTMLElement>document.activeElement;
            var coords: number[] = gridImpl.getLocationDependentCoordinatesFor(active);
            if (coords == null) return;
            var r: number = coords[0];
            var c: number = coords[1];
            gridImpl.focusCell(c, r);
        }, 0);
    }

    /**
     * Called when a cell in the main table body is clicked
     */
    public cellClicked(gridImpl: grid.Grid, cell: HTMLElement, column: number, row: number, event: MouseEvent) {

        if (column == 0 || gridImpl.getNavigationLocation() != grid.Grid.NAVIGATION_BODY) {
            // Don't trigger actionable mode on checkbox column
            return;
        }

        if (gridImpl.getNavigationMode() == grid.Grid.NAVIGATION_MODE_NAVIGATION) {
            if (event.detail == 2) {

                this.oldValues = new Array<string>();

                // Double click enters actionable mode
                gridImpl.setNavigationMode(grid.Grid.NAVIGATION_MODE_ACTIONABLE);

                // Replace cells with input fields
                for (var columnIndex: number = 1; columnIndex < gridImpl.getColumnCount(); columnIndex++) {
                    var c: HTMLElement = gridImpl.getCell(columnIndex, row);

                    // Store old value
                    this.oldValues.push(c.innerHTML);

                    // Replace content with input field
                    if (columnIndex == 4) {
                        ActionableModeBehavior.replaceContentWithSelect(c, PersonnelGenderString);

                    } else if (!ActionableModeBehavior.isInlineWidgetCell(c)) {
                        ActionableModeBehavior.replaceContentWithField(c);
                    }

                    // Add style name
                    util.addStyleName("actionable", c);
                }

                // Focus content
                try {
                    if (!ActionableModeBehavior.isInlineWidgetCell(cell)) {
                        var input: HTMLInputElement = <HTMLInputElement> cell.firstElementChild;
                        input.focus();
                        input.setSelectionRange(0, input.value.length);
                    } else {
                        var input: HTMLInputElement = <HTMLInputElement> cell.firstElementChild.firstElementChild;
                        input.focus();
                    }
                } catch (e) { }

                this.showEditor(cell);
            }
        }
    }
}