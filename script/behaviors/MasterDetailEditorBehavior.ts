class MasterDetailEditorBehavior extends grid.Behavior {

    private form: HTMLFormElement = <HTMLFormElement> document.getElementById("master-detail-form");

    private g: grid.Grid;
    private currentRow: number;

    public constructor() {
        super();
        this.form.onreset = (e) => {
            this.reset();
            e.preventDefault();
        }
        this.form.onsubmit = (e) => {
            this.submit();
            e.preventDefault();
        }
    }

    public focusChanged(g: grid.Grid, cell: HTMLElement): void {
        if (cell != null && g.getNavigationLocation() == grid.Grid.NAVIGATION_BODY) {

            this.g = g;
            this.form.parentElement.style.display = "block";

            // Find row            
            var row = this.currentRow = g.getLocationDependentCoordinatesFor(cell)[0];

            // Populate master-detail
            (<HTMLInputElement>this.form.elements.item(0)).value = g.getCell(1, row).textContent;
            (<HTMLInputElement>this.form.elements.item(1)).value = g.getCell(2, row).textContent;
            (<HTMLInputElement>this.form.elements.item(2)).value = g.getCell(3, row).textContent;
            (<HTMLInputElement>this.form.elements.item(3)).value = "" + PersonnelGenderString.indexOf(g.getCell(4, row).textContent);
            (<HTMLInputElement>this.form.elements.item(4)).checked = g.getCell(5, row).getElementsByTagName("input")[0].checked;

        } else {
            this.form.parentElement.style.display = "none";
        }
    }

    public submit() {
        var g = this.g;
        var row = this.currentRow;

        var name = (<HTMLInputElement>this.form.elements.item(0)).value;
        var lastname = (<HTMLInputElement>this.form.elements.item(1)).value;
        var email = (<HTMLInputElement>this.form.elements.item(2)).value;
        var gender = PersonnelGenderString[parseInt((<HTMLInputElement>this.form.elements.item(3)).value)];
        var favourite = (<HTMLInputElement>this.form.elements.item(4)).checked;

        g.getCell(1, row).innerHTML = "<span>" + name + "</span>";
        g.getCell(2, row).innerHTML = "<span>" + lastname + "</span>";
        g.getCell(3, row).innerHTML = "<span>" + email + "</span>";
        g.getCell(4, row).innerHTML = "<span>" + gender + "</span>";
        g.getCell(5, row).getElementsByTagName("input")[0].checked = favourite;
    }
    
    public reset() {
        var g = this.g;
        var cell = g.getFocusedCell();
        if (cell != null && g.getNavigationLocation() == grid.Grid.NAVIGATION_BODY) {

            this.g = g;
            this.form.parentElement.style.display = "block";

            // Find row            
            var row = this.currentRow = g.getLocationDependentCoordinatesFor(cell)[0];

            // Populate master-detail
            (<HTMLInputElement>this.form.elements.item(0)).value = g.getCell(1, row).textContent;
            (<HTMLInputElement>this.form.elements.item(1)).value = g.getCell(2, row).textContent;
            (<HTMLInputElement>this.form.elements.item(2)).value = g.getCell(3, row).textContent;
            (<HTMLInputElement>this.form.elements.item(3)).value = "" + PersonnelGenderString.indexOf(g.getCell(4, row).textContent);
            (<HTMLInputElement>this.form.elements.item(4)).checked = g.getCell(5, row).getElementsByTagName("input")[0].checked;

        } else {
            this.form.parentElement.style.display = "none";
        }
    }
}
