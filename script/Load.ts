/**
 * Load a bunch of files, roughly in order.
 */
function load(files: string[], onComplete: Function = null) {
    (function loadNext() {
        function loadFile(basepath: string): void {
            var script = document.createElement('script');
            script.type = "text/javascript";
            script.src = "script/" + basepath + ".js";
            document.head.appendChild(script);
        }

        if (files.length) {
            setTimeout(() => {
                loadFile(files.shift());
                loadNext();
            }, 1);
        } else {
            if (onComplete !== null) {
                onComplete();
            }
        }
    })();
}

// ========================
//  Load dependent scripts
// ========================

// Add files you wish to load to this array
load([
    'Util',
    'KeyCodes',
    'grid/Table',
    'grid/Grid',
    'grid/Behavior',
    'behaviors/SingleSortingBehavior',
    'behaviors/MultiSortingBehavior',
    'behaviors/NavigationModeBehavior',
    'behaviors/ActionableModeBehavior',
    'behaviors/MultiSelectionBehavior',
    'behaviors/SingleSelectionBehavior',
    'behaviors/TouchContextMenuBehavior',
    //'behaviors/KeyboardContextMenuBehavior',
    'behaviors/BodyTouchScrollBehavior',
    'behaviors/RowEditorBehavior',
    'behaviors/MasterDetailEditorBehavior',
    
    'data/PersonnelData'
    
], function() {
        // Load main script last to avoid reload failure
        load(['Main']);
    });
