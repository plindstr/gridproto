enum KeyCodes {
    ARROW_UP = 38,
    ARROW_DOWN = 40,
    ARROW_LEFT = 37,
    ARROW_RIGHT = 39,
    PG_UP = 33,
    PG_DOWN = 34,
    HOME = 36,
    END = 35,
    
    TAB = 9,
    ENTER = 13,
    ESCAPE = 27,
    SPACE = 32,
    SHIFT = 16,
    F8 = 119,
    F2 = 113,
    F10 = 121
   
}
