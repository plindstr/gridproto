
var PersonnelHeaderData: string[] = [
    "Name", "Surname", "E-Mail", "Gender", "Favourite"
];

var PersonnelFooterData: string[] = [
    "Name", "Surname", "E-Mail", "Gender", "Favourite"
];

enum PersonnelGender {
    Male = 0, Female = 1, Other = 3
}

var PersonnelGenderString: string[] = [
    "Male", "Female", "Other"  
];

var PersonnelData: string[][] = [
    ["Amir", "Al-Majdalawi", "amir@vaadin.com", "#OPTION(0)#", "#BOOL(0)#"],
    ["Anna", "Koskinen", "anna@vaadin.com", "#OPTION(1)#", "#BOOL(0)#"],
    ["Antti", "Tanhuanpää", "antti@vaadin.com", "#OPTION(0)#", "#BOOL(0)#"],
    ["Artem", "Godin", "artem@vaadin.com", "#OPTION(0)#", "#BOOL(0)#"],
    ["Artur", "Signell", "artur@vaadin.com", "#OPTION(0)#", "#BOOL(0)#"],
    ["Denis", "Anisimov", "denis@vaadin.com", "#OPTION(0)#", "#BOOL(0)#"],
    ["Felype", "Ferreira", "felype@vaadin.com", "#OPTION(0)#", "#BOOL(0)#"],
    ["Fredrik", "Rönnlund", "fredu@vaadin.com", "#OPTION(0)#", "#BOOL(0)#"],
    ["Haijian", "Wang", "haijian@vaadin.com", "#OPTION(0)#", "#BOOL(0)#"],
    ["Hannu", "Salonen", "hannu@vaadin.com", "#OPTION(0)#", "#BOOL(0)#"],
    ["Henri", "Kerola", "henri.kerola@vaadin.com", "#OPTION(0)#", "#BOOL(0)#"],
    ["Henri", "Sara", "hesara@vaadin.com", "#OPTION(0)#", "#BOOL(0)#"],
    ["Henri", "Muurimaa", "henri@vaadin.com", "#OPTION(0)#", "#BOOL(0)#"],
    ["Henrik", "Paul", "henrik@vaadin.com", "#OPTION(0)#", "#BOOL(0)#"],
    ["Jani", "Laakso", "jani@vaadin.com", "#OPTION(0)#", "#BOOL(0)#"],
    ["Jarno", "Rantala", "jarnor@vaadin.com", "#OPTION(0)#", "#BOOL(0)#"],
    ["Jens", "Jansson", "jens@vaadin.com", "#OPTION(0)#", "#BOOL(0)#"],
    ["Joacim", "Päivärinne", "joacim@vaadin.com", "#OPTION(0)#", "#BOOL(0)#"],
    ["Johan", "Ånäs", "johan@vaadin.com", "#OPTION(0)#", "#BOOL(0)#"],
    ["Johannes", "Eriksson", "joheriks@vaadin.com", "#OPTION(0)#", "#BOOL(0)#"],
    ["Johannes", "Tuikkala", "johannes.tuikkala@vaadin.com", "#OPTION(0)#", "#BOOL(0)#"],
    ["Johannes", "Häyry", "johannes.hayry@vaadin.com", "#OPTION(0)#", "#BOOL(0)#"],
    ["Johannes", "Dahlström", "johannesd@vaadin.com", "#OPTION(0)#", "#BOOL(0)#"],
    ["John", "Ahlroos", "john@vaadin.com", "#OPTION(0)#", "#BOOL(0)#"],
    ["Jonas", "Granvik", "jonas@vaadin.com", "#OPTION(0)#", "#BOOL(0)#"],
    ["Jonatan", "Kronqvist", "jonatan@vaadin.com", "#OPTION(0)#", "#BOOL(0)#"],
    ["Jonni", "Nakari", "jonni@vaadin.com", "#OPTION(0)#", "#BOOL(0)#"],
    ["Joonas", "Lehtinen", "joonas@vaadin.com", "#OPTION(0)#", "#BOOL(0)#"],
    ["Jouni", "Koivuviita", "jouni@vaadin.com", "#OPTION(0)#", "#BOOL(0)#"],
    ["Juho", "Nurminen", "juho@vaadin.com", "#OPTION(0)#", "#BOOL(0)#"],
    ["Jurka", "Rahikkala", "jurka@vaadin.com", "#OPTION(0)#", "#BOOL(0)#"],
    ["Kim", "Leppänen", "kim@vaadin.com", "#OPTION(0)#", "#BOOL(0)#"],
    ["Leif", "Åstrand", "leif@vaadin.com", "#OPTION(0)#", "#BOOL(0)#"],
    ["Maciej", "Przepiora", "matthew@vaadin.com", "#OPTION(0)#", "#BOOL(0)#"],
    ["Manuel", "Carrasco", "manolo@vaadin.com", "#OPTION(0)#", "#BOOL(0)#"],
    ["Marc", "Englund", "marc@vaadin.com", "#OPTION(0)#", "#BOOL(0)#"],
    ["Marcus", "Hellberg", "marcus@vaadin.com", "#OPTION(0)#", "#BOOL(0)#"],
    ["Marko", "Grönroos", "magi@vaadin.com", "#OPTION(0)#", "#BOOL(0)#"],
    ["Marlon", "Richert", "marlon@vaadin.com", "#OPTION(0)#", "#BOOL(0)#"],
    ["Matti", "Tahvonen", "matti@vaadin.com", "#OPTION(0)#", "#BOOL(0)#"],
    ["Matti", "Vesa", "matti.vesa@vaadin.com", "#OPTION(0)#", "#BOOL(0)#"],
    ["Matti", "Hosio", "matti.hosio@vaadin.com", "#OPTION(0)#", "#BOOL(0)#"],
    ["Michael", "Tzukanov", "michael.tzukanov@vaadin.com", "#OPTION(0)#", "#BOOL(0)#"],
    ["Mika", "Murtojärvi", "mika.murtojarvi@vaadin.com", "#OPTION(0)#", "#BOOL(0)#"],
    ["Mikael", "Vappula", "mikael@vaadin.com", "#OPTION(0)#", "#BOOL(0)#"],
    ["Mikael", "Grankvist", "mikael.grankvist@vaadin.com", "#OPTION(0)#", "#BOOL(0)#"],
    ["Mikołaj", "Olszewski", "miki@vaadin.com", "#OPTION(0)#", "#BOOL(0)#"],
    ["Minna", "Kuoppala", "minna@vaadin.com", "#OPTION(0)#", "#BOOL(0)#"],
    ["Olli", "Helttula", "olli@vaadin.com", "#OPTION(0)#", "#BOOL(0)#"],
    ["Otto", "Järvinen", "otto@vaadin.com", "#OPTION(0)#", "#BOOL(0)#"],
    ["Patrik", "Lindström", "patrik@vaadin.com", "#OPTION(0)#", "#BOOL(0)#"],
    ["Pekka", "Hyvönen", "pekka@vaadin.com", "#OPTION(0)#", "#BOOL(0)#"],
    ["Pekka", "Perälä", "pekkap@vaadin.com", "#OPTION(0)#", "#BOOL(0)#"],
    ["Peter", "Lehto", "peter@vaadin.com", "#OPTION(0)#", "#BOOL(0)#"],
    ["Petri", "Heinonen", "petri@vaadin.com", "#OPTION(0)#", "#BOOL(0)#"],
    ["Petter", "Holmström", "petter@vaadin.com", "#OPTION(0)#", "#BOOL(0)#"],
    ["Risto", "Yrjänä", "risto@vaadin.com", "#OPTION(0)#", "#BOOL(0)#"],
    ["Rolf", "Smeds", "rolf@vaadin.com", "#OPTION(0)#", "#BOOL(0)#"],
    ["Sami", "Ekblad", "sami@vaadin.com", "#OPTION(0)#", "#BOOL(0)#"],
    ["Sami", "Kaksonen", "samik@vaadin.com", "#OPTION(0)#", "#BOOL(0)#"],
    ["Sami", "Viitanen", "sami.viitanen@vaadin.com", "#OPTION(0)#", "#BOOL(0)#"],
    ["Samuli", "Penttilä", "samuli@vaadin.com", "#OPTION(0)#", "#BOOL(0)#"],
    ["Sauli", "Tähkäpää", "sauli@vaadin.com", "#OPTION(0)#", "#BOOL(0)#"],
    ["Sebastian", "Nyholm", "sebastian@vaadin.com", "#OPTION(0)#", "#BOOL(0)#"],
    ["Tanja", "Repo", "tanja@vaadin.com", "#OPTION(1)#", "#BOOL(0)#"],
    ["Tapio", "Aali", "tapio@vaadin.com", "#OPTION(0)#", "#BOOL(0)#"],
    ["Teemu", "Pöntelin", "teemu@vaadin.com", "#OPTION(0)#", "#BOOL(0)#"],
    ["Teemu", "Suo-Anttila", "teemu.suoanttila@vaadin.com", "#OPTION(0)#", "#BOOL(0)#"],
    ["Teppo", "Kurki", "teppo@vaadin.com", "#OPTION(0)#", "#BOOL(0)#"],
    ["Thomas", "Mattsson", "thomas@vaadin.com", "#OPTION(0)#", "#BOOL(0)#"],
    ["Tiina", "Wasberg", "tiina@vaadin.com", "#OPTION(1)#", "#BOOL(0)#"],
    ["Tomi", "Virtanen", "tomi.virtanen@vaadin.com", "#OPTION(0)#", "#BOOL(0)#"],
    ["Tomi", "Virkki", "virkki@vaadin.com", "#OPTION(0)#", "#BOOL(0)#"],
    ["Ville", "Ingman", "ville@vaadin.com", "#OPTION(0)#", "#BOOL(0)#"],
    ["Jarmo", "Kemppainen", "jarmo@vaadin.com", "#OPTION(0)#", "#BOOL(0)#"],
    ["Juuso", "Valli", "juuso@vaadin.com", "#OPTION(0)#", "#BOOL(0)#"],
    ["Dmitrii", "Rogozin", "dmitrii@vaadin.com", "#OPTION(0)#", "#BOOL(0)#"],
    ["Henrik", "Skogström", "hmskog@vaadin.com", "#OPTION(0)#", "#BOOL(0)#"]
];
