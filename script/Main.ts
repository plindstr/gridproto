/**
 * Entry point for Grid user interaction prototype code
 * 
 * (c) Vaadin 2014
 */

// =================
//  Initialize demo
// =================





// Create a 10 column, 256 line grid filled with randomness.
var mygrid = new grid.Grid(690);
//mygrid.initWithBogusData(9,300);
mygrid.initWithData(PersonnelHeaderData,PersonnelData,PersonnelFooterData);


mygrid.addBehavior(new NavigationModeBehavior());
mygrid.addBehavior(new MultiSelectionBehavior());
//mygrid.addBehavior(new SingleSelectionBehavior());

/*
 * Sorting modes
 */ 
//mygrid.addBehavior(new SingleSortingBehavior());
mygrid.addBehavior(new MultiSortingBehavior());

mygrid.addBehavior(new TouchContextMenuBehavior(mygrid));
mygrid.addBehavior(new BodyTouchScrollBehavior());


/*
 * Edit mode (select one)
 */ 
//mygrid.addBehavior(new ActionableModeBehavior());
mygrid.addBehavior(new RowEditorBehavior(mygrid));
//mygrid.addBehavior(new MasterDetailEditorBehavior()); 

var removeButton = <HTMLButtonElement>document.getElementById('remove-selected');
removeButton.onclick = (e) => {
    mygrid.removeSelectedRows();
};


var container = document.getElementById("gridcontainer");
container.appendChild(mygrid.getElement());
